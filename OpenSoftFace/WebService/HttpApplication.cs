﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace OpenSoftFace.WebService
{
    public class HttpApplication : IHttpHandler
    {
        // 对请求上下文进行处理
        public void ProcessRequest(HttpContext context)
        {
            // 1.获取网站根路径
            if(string.IsNullOrEmpty(context.Request.Url))
            {
                return;
            }
            string bastPath = AppDomain.CurrentDomain.BaseDirectory;
            string fileName = Path.Combine(bastPath+"\\MyWebSite", context.Request.Url.TrimStart('/'));
            string fileExtension = Path.GetExtension(context.Request.Url);

            // 2.处理动态文件请求
            //|| !fileExtension.Equals(".")
            if (fileExtension.Equals("Api") || context.Request.Url.Contains("Api"))
            {
                //string className = Path.GetFileNameWithoutExtension(context.Request.Url);
                //if (className == "") className = "Api";
                string className = "Api";
                IHttpHandler handler = Assembly.Load("OpenSoftFace").CreateInstance("OpenSoftFace.WebService." + className) as IHttpHandler;
                handler.ProcessRequest(context);
                return;
            }
            // 3.处理静态文件请求
            if (!File.Exists(fileName))
            {
                context.Response.StateCode = "404";
                context.Response.StateDescription = "Not Found";
                context.Response.ContentType = "text/html";
                string notExistHtml = Path.Combine(bastPath, @"MyWebSite\notfound.html");
                context.Response.Body = File.ReadAllBytes(notExistHtml);
            }
            else
            {
                context.Response.StateCode = "200";
                context.Response.StateDescription = "OK";
                context.Response.ContentType = GetContenType(Path.GetExtension(context.Request.Url));
                context.Response.Body = File.ReadAllBytes(fileName);
            } 
        }

        // 根据文件扩展名获取内容类型
        public string GetContenType(string fileExtension)
        {
            string type = "text/html; charset=UTF-8";
            switch (fileExtension)
            {
                case ".aspx":
                case ".html":
                case ".htm":
                    type = "text/html; charset=UTF-8";
                    break;
                case ".png":
                    type = "image/png";
                    break;
                case ".gif":
                    type = "image/gif";
                    break;
                case ".jpg":
                case ".jpeg":
                    type = "image/jpeg";
                    break;
                case ".css":
                    type = "text/css";
                    break;
                case ".js":
                    type = "application/x-javascript";
                    break;
                default:
                    type = "text/plain; charset=gbk";
                    break;
            }
            return type;
        }
    }
}
