
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MySwitch : UserControl
	{
		private bool _isOpen;

		private List<Control> controlList = new List<Control>();

		private IContainer components = null;

		[Browsable(true), Category("SwitchStatus"), Description("开关状态")]
		public bool SwitchStatus
		{
			get
			{
				return this._isOpen;
			}
			set
			{
				this._isOpen = value;
				this.init();
			}
		}

		public MySwitch()
		{
			this.InitializeComponent();
		}

		private void init()
		{
			this.enabledControls(this._isOpen);
			bool isOpen = this._isOpen;
			if (isOpen)
			{
				//this.BackgroundImage = Resources.open_switch;
			}
			else
			{
				//this.BackgroundImage = Resources.close_switch;
			}
		}

		private void Switch_Click(object sender, EventArgs e)
		{
			this._isOpen = !this._isOpen;
			this.init();
		}

		private void Switch_MouseMove(object sender, MouseEventArgs e)
		{
			this.Cursor = Cursors.Hand;
		}

		public void BindControls(params Control[] controls)
		{
			this.controlList.Clear();
			bool flag = controls != null && controls.Length != 0;
			if (flag)
			{
				this.controlList.AddRange(controls);
			}
			this.init();
		}

		private void enabledControls(bool isEnabled)
		{
			bool flag = this.controlList != null && this.controlList.Count > 0;
			if (flag)
			{
				foreach (Control current in this.controlList)
				{
					current.Enabled = isEnabled;
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = Color.Transparent;
			//this.BackgroundImage = Resources.close_switch;
			this.BackgroundImageLayout = ImageLayout.Zoom;
			this.DoubleBuffered = true;
			base.Margin = new Padding(0);
			base.Name = "Switch";
			base.Size = new Size(45, 15);
			base.Click += new EventHandler(this.Switch_Click);
			base.MouseMove += new MouseEventHandler(this.Switch_MouseMove);
			base.ResumeLayout(false);
		}
	}
}
