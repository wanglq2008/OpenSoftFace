using CommonConstant;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MyVerticalTab : TabControl
	{
		private int _leftTabIndex = 0;

		[Category("LeftTabIndex"), Description("设置tab标签文字距离左边界的大小")]
		public int LeftTabIndex
		{
			get
			{
				return this._leftTabIndex;
			}
			set
			{
				this._leftTabIndex = value;
				base.Invalidate();
			}
		}

		public override Rectangle DisplayRectangle
		{
			get
			{
				Rectangle displayRectangle = base.DisplayRectangle;
				return new Rectangle(displayRectangle.Left - 4, displayRectangle.Top - 4, displayRectangle.Width + 8, displayRectangle.Height + 8);
			}
		}

		public MyVerticalTab()
		{
			this.SetStyles();
			this.TabSet();
		}

		private void SetStyles()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);
			base.UpdateStyles();
		}

		private void TabSet()
		{
			base.DrawMode = TabDrawMode.OwnerDrawFixed;
			base.SizeMode = TabSizeMode.Fixed;
			base.Multiline = true;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			try
			{
				Rectangle clientRectangle = base.ClientRectangle;
				e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
				e.Graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
				using (BufferedGraphics bufferedGraphics = BufferedGraphicsManager.Current.Allocate(e.Graphics, clientRectangle))
				{
                    bufferedGraphics.Graphics.FillRectangle(new SolidBrush(ColorUtil.Blue), clientRectangle);
					for (int i = 0; i < base.TabCount; i++)
					{
						this.DrawTabPage(bufferedGraphics.Graphics, base.GetTabRect(i), i);
					}
					bufferedGraphics.Render(e.Graphics);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void DrawTabPage(Graphics graphics, Rectangle rectangle, int index)
		{
			graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
			Font font = new Font("微软雅黑", 15f, FontStyle.Bold, GraphicsUnit.Point, 134);
			StringFormat stringFormat = new StringFormat();
			stringFormat.LineAlignment = StringAlignment.Center;
			stringFormat.Alignment = StringAlignment.Center;
			Rectangle r = rectangle;
			r.X += this.LeftTabIndex;
			try
			{
				bool flag = index == base.SelectedIndex;
				if (flag)
				{
                    graphics.FillPath(new SolidBrush(ColorUtil.Blue), this.CreateTabPath(rectangle));
					graphics.DrawString(base.TabPages[index].Text, font, new SolidBrush(ColorUtil.TabSelectFore), r, stringFormat);
				}
				else
				{
                    graphics.FillPath(new SolidBrush(ColorUtil.Blue), this.CreateTabPath(rectangle));
					graphics.DrawString(base.TabPages[index].Text, font, new SolidBrush(ColorUtil.White), r, stringFormat);
				}
			}
			catch (NullReferenceException ex)
			{
				LogHelper.LogError(base.GetType(), ex);
				Console.WriteLine(ex);
			}
		}

		private GraphicsPath CreateTabPath(Rectangle rect)
		{
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddLine(rect.Left - 1, rect.Top, rect.Left - 1, rect.Top);
			graphicsPath.AddLine(rect.Left - 1, rect.Top, rect.Right, rect.Top);
			graphicsPath.AddLine(rect.Right, rect.Top, rect.Right, rect.Bottom);
			graphicsPath.AddLine(rect.Right, rect.Bottom + 1, rect.Left - 1, rect.Bottom + 1);
			graphicsPath.CloseFigure();
			return graphicsPath;
		}
	}
}
