using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	[Description("提供一个Vista样式的选择文件对话框"), Editor(typeof(MyFolderNameEditor), typeof(UITypeEditor))]
	public class MyFolderBrowserDialog : Component
	{
		[Guid("DC1C5A9C-E88A-4dde-A5A1-60F82A20AEF7")]
		[ComImport]
		private class FileOpenDialog
		{
			//[MethodImpl(MethodImplOptions.InternalCall)]
			//public extern FileOpenDialog();
		}

		[Guid("42f85136-db7e-439c-85f1-e4075d135fc8"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		[ComImport]
		private interface IFileOpenDialog
		{
			[PreserveSig]
			uint Show([In] IntPtr parent);

			void SetFileTypes();

			void SetFileTypeIndex([In] uint iFileType);

			void GetFileTypeIndex(out uint piFileType);

			void Advise();

			void Unadvise();

			void SetOptions([In] MyFolderBrowserDialog.FOS fos);

			void GetOptions(out MyFolderBrowserDialog.FOS pfos);

			void SetDefaultFolder(MyFolderBrowserDialog.IShellItem psi);

			void SetFolder(MyFolderBrowserDialog.IShellItem psi);

			void GetFolder(out MyFolderBrowserDialog.IShellItem ppsi);

			void GetCurrentSelection(out MyFolderBrowserDialog.IShellItem ppsi);

			void SetFileName([MarshalAs(UnmanagedType.LPWStr)] [In] string pszName);

			void GetFileName([MarshalAs(UnmanagedType.LPWStr)] out string pszName);

			void SetTitle([MarshalAs(UnmanagedType.LPWStr)] [In] string pszTitle);

			void SetOkButtonLabel([MarshalAs(UnmanagedType.LPWStr)] [In] string pszText);

			void SetFileNameLabel([MarshalAs(UnmanagedType.LPWStr)] [In] string pszLabel);

			void GetResult(out MyFolderBrowserDialog.IShellItem ppsi);

			void AddPlace(MyFolderBrowserDialog.IShellItem psi, int alignment);

			void SetDefaultExtension([MarshalAs(UnmanagedType.LPWStr)] [In] string pszDefaultExtension);

			void Close(int hr);

			void SetClientGuid();

			void ClearClientData();

			void SetFilter([MarshalAs(UnmanagedType.Interface)] IntPtr pFilter);

			void GetResults([MarshalAs(UnmanagedType.Interface)] out IntPtr ppenum);

			void GetSelectedItems([MarshalAs(UnmanagedType.Interface)] out IntPtr ppsai);
		}

		[Guid("43826D1E-E718-42EE-BC55-A1E261C37BFE"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		[ComImport]
		private interface IShellItem
		{
			void BindToHandler();

			void GetParent();

			void GetDisplayName([In] MyFolderBrowserDialog.SIGDN sigdnName, [MarshalAs(UnmanagedType.LPWStr)] out string ppszName);

			void GetAttributes();

			void Compare();
		}

		private enum SIGDN : uint
		{
			SIGDN_DESKTOPABSOLUTEEDITING = 2147794944u,
			SIGDN_DESKTOPABSOLUTEPARSING = 2147647488u,
			SIGDN_FILESYSPATH = 2147844096u,
			SIGDN_NORMALDISPLAY = 0u,
			SIGDN_PARENTRELATIVE = 2148007937u,
			SIGDN_PARENTRELATIVEEDITING = 2147684353u,
			SIGDN_PARENTRELATIVEFORADDRESSBAR = 2147991553u,
			SIGDN_PARENTRELATIVEPARSING = 2147581953u,
			SIGDN_URL = 2147909632u
		}

		[Flags]
		private enum FOS
		{
			FOS_ALLNONSTORAGEITEMS = 128,
			FOS_ALLOWMULTISELECT = 512,
			FOS_CREATEPROMPT = 8192,
			FOS_DEFAULTNOMINIMODE = 536870912,
			FOS_DONTADDTORECENT = 33554432,
			FOS_FILEMUSTEXIST = 4096,
			FOS_FORCEFILESYSTEM = 64,
			FOS_FORCESHOWHIDDEN = 268435456,
			FOS_HIDEMRUPLACES = 131072,
			FOS_HIDEPINNEDPLACES = 262144,
			FOS_NOCHANGEDIR = 8,
			FOS_NODEREFERENCELINKS = 1048576,
			FOS_NOREADONLYRETURN = 32768,
			FOS_NOTESTFILECREATE = 65536,
			FOS_NOVALIDATE = 256,
			FOS_OVERWRITEPROMPT = 2,
			FOS_PATHMUSTEXIST = 2048,
			FOS_PICKFOLDERS = 32,
			FOS_SHAREAWARE = 16384,
			FOS_STRICTFILETYPES = 4
		}

		private const uint ERROR_CANCELLED = 2147943623u;

		public string DirectoryPath
		{
			get;
			set;
		}

		public DialogResult ShowDialog(IWin32Window owner)
		{
			IntPtr parent = (owner != null) ? owner.Handle : MyFolderBrowserDialog.GetActiveWindow();
			MyFolderBrowserDialog.IFileOpenDialog fileOpenDialog = (MyFolderBrowserDialog.IFileOpenDialog)new MyFolderBrowserDialog.FileOpenDialog();
			DialogResult result;
			try
			{
				bool flag = !string.IsNullOrEmpty(this.DirectoryPath);
				if (flag)
				{
					uint num = 0u;
					IntPtr pidl;
					bool flag2 = MyFolderBrowserDialog.SHILCreateFromPath(this.DirectoryPath, out pidl, ref num) == 0;
					if (flag2)
					{
						MyFolderBrowserDialog.IShellItem shellItem;
						bool flag3 = MyFolderBrowserDialog.SHCreateShellItem(IntPtr.Zero, IntPtr.Zero, pidl, out shellItem) == 0;
						if (flag3)
						{
							fileOpenDialog.SetFolder(shellItem);
						}
					}
				}
				fileOpenDialog.SetOptions(MyFolderBrowserDialog.FOS.FOS_FORCEFILESYSTEM | MyFolderBrowserDialog.FOS.FOS_PICKFOLDERS);
				uint num2 = fileOpenDialog.Show(parent);
				bool flag4 = num2 == 2147943623u;
				if (flag4)
				{
					result = DialogResult.Cancel;
				}
				else
				{
					bool flag5 = num2 > 0u;
					if (flag5)
					{
						result = DialogResult.Abort;
					}
					else
					{
						MyFolderBrowserDialog.IShellItem shellItem;
						fileOpenDialog.GetResult(out shellItem);
						string directoryPath;
						shellItem.GetDisplayName((MyFolderBrowserDialog.SIGDN)2147844096u, out directoryPath);
						this.DirectoryPath = directoryPath;
						result = DialogResult.OK;
					}
				}
			}
			finally
			{
				Marshal.ReleaseComObject(fileOpenDialog);
			}
			return result;
		}

		[DllImport("shell32.dll")]
		private static extern int SHILCreateFromPath([MarshalAs(UnmanagedType.LPWStr)] string pszPath, out IntPtr ppIdl, ref uint rgflnOut);

		[DllImport("shell32.dll")]
		private static extern int SHCreateShellItem(IntPtr pidlParent, IntPtr psfParent, IntPtr pidl, out MyFolderBrowserDialog.IShellItem ppsi);

		[DllImport("user32.dll")]
		private static extern IntPtr GetActiveWindow();
	}
}
