using OpenSoftFace.Setting.Models;
using System;
using System.Runtime.InteropServices;

namespace OpenSoftFace.Setting.Utils
{
	public class ASFFunctions
	{
		public const string Dll_PATH = "libarcsoft_face_engine.dll";

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFActivation(string appId, string sdkKey);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFOnlineActivation(string appId, string sdkKey);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFInitEngine(DetectionMode detectMode, ASF_OrientPriority detectFaceOrientPriority, int detectFaceScaleVal, int detectFaceMaxNum, int combinedMask, ref IntPtr pEngine);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFDetectFaces(IntPtr pEngine, int width, int height, int format, IntPtr imgData, IntPtr detectedFaces, ASF_DetectModel detectModel = ASF_DetectModel.ASF_DETECT_MODEL_RGB);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFProcess(IntPtr pEngine, int width, int height, int format, IntPtr imgData, IntPtr detectedFaces, int combinedMask);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFFaceFeatureExtract(IntPtr pEngine, int width, int height, int format, IntPtr imgData, IntPtr faceInfo, IntPtr faceFeature);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFFaceFeatureCompare(IntPtr pEngine, IntPtr faceFeature1, IntPtr faceFeature2, ref float similarity, ASF_CompareModel compareModel = ASF_CompareModel.ASF_LIFE_PHOTO);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFGetAge(IntPtr pEngine, IntPtr ageInfo);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFGetGender(IntPtr pEngine, IntPtr genderInfo);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFGetFace3DAngle(IntPtr pEngine, IntPtr p3DAngleInfo);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFGetLivenessScore(IntPtr hEngine, IntPtr livenessInfo);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFProcess_IR(IntPtr pEngine, int width, int height, int format, IntPtr imgData, IntPtr faceInfo, int combinedMask);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFGetLivenessScore_IR(IntPtr pEngine, IntPtr irLivenessInfo);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFUninitEngine(IntPtr pEngine);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern ASF_VERSION ASFGetVersion();

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFGetActiveFileInfo(IntPtr activeFileInfo);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFDetectFacesEx(IntPtr hEngine, IntPtr imgData, IntPtr detectedFaces, ASF_DetectModel detectModel = ASF_DetectModel.ASF_DETECT_MODEL_RGB);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFSetLivenessParam(IntPtr hEngine, IntPtr threshold);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFProcessEx(IntPtr hEngine, IntPtr imgData, IntPtr detectedFaces, int combinedMask);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFProcessEx_IR(IntPtr hEngine, IntPtr imgData, IntPtr detectedFaces, int combinedMask);

		[DllImport("libarcsoft_face_engine.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ASFFaceFeatureExtractEx(IntPtr hEngine, IntPtr imgData, IntPtr faceInfo, IntPtr feature);
	}
}
