using OpenSoftFace.Setting.Utils;
using DAL.Service;
using DAL.Service.Impl;
using LogUtil;
using Model.Entity;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms.BacthImport
{
	public class Import : Form
	{
		private IMainInfoService mainInfoService = new MainInfoServiceImpl();

		private IFeatureInfoService featureInfoService = new FeatureInfoServiceImpl();

		private IContainer components = null;

		private Label label1;

		private TextBox txt_path;

		private Button btn_select;

		private Label lbl;

		private Button btn_import;

		private PictureBox pictureBox1;

		public Import()
		{
			this.InitializeComponent();
		}

		private void btn_select_Click(object sender, EventArgs e)
		{
			try
			{
				FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
				bool flag = folderBrowserDialog.ShowDialog() == DialogResult.OK;
				if (flag)
				{
					this.txt_path.Text = folderBrowserDialog.SelectedPath;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void btn_import_Click(object sender, EventArgs e)
		{
			try
			{
				bool flag = string.IsNullOrEmpty(this.txt_path.Text) || !Directory.Exists(this.txt_path.Text);
				if (flag)
				{
					MessageBox.Show("图片路径为空或不存在");
				}
				else
				{
					string[] images = Directory.GetFiles(this.txt_path.Text, "*.jpg");
					bool flag2 = images == null || images.Length == 0;
					if (flag2)
					{
						MessageBox.Show("图片路径下不存在*.jpg图像");
					}
					else
					{
						string showPath = Application.StartupPath + "\\show\\";
						string text = DateTime.Now.ToString("MMddHHmm");
						try
						{
							bool flag3 = !Directory.Exists(showPath);
							if (flag3)
							{
								Directory.CreateDirectory(showPath);
							}
						}
						catch
						{
						}
						this.enabledControls(false);
						ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
						{
							ControlUtil.LabelTextUpdate(this.lbl, string.Empty);
							int num = 1;
							//string[] images;
							int num2 = images.Length;
							//images = images;
							int i = 0;
							while (i < images.Length)
							{
								string filePath = images[i];
								try
								{
									string empty = string.Empty;
									Image image = null;
									FeatureInfo featureInfo = SDKOperate.DetectAndRecognitionImage(filePath, ref empty, ref image);
									bool flag4 = !string.IsNullOrEmpty(empty);
									if (flag4)
									{
										goto IL_248;
									}
									MainInfo mainInfo = new MainInfo();
									int num3 = this.mainInfoService.MaxId();
									mainInfo.PersonName = num3.ToString();
									mainInfo.PersonSerial = num3.ToString();
									mainInfo.Dept = string.Empty;
									mainInfo.ID = num3;
									mainInfo.ShowImageName = string.Format("show_{0}_{1}.jpg", num3, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
									try
									{
										image.Save(showPath + mainInfo.ShowImageName, ImageFormat.Jpeg);
									}
									catch (Exception se2)
									{
										LogHelper.LogError(this.GetType(), se2);
									}
									bool flag5 = featureInfo != null;
									if (flag5)
									{
										mainInfo.PTop = featureInfo.FTop;
										mainInfo.PLeft = featureInfo.FLeft;
										mainInfo.PRight = featureInfo.FRight;
										mainInfo.PBottom = featureInfo.FBottom;
										mainInfo.POrient = featureInfo.FOrient;
									}
									mainInfo.PValid = 1;
									int num4 = this.mainInfoService.Insert(mainInfo);
									bool flag6 = num4 > 0;
									if (flag6)
									{
										featureInfo.PersonId = num3;
										featureInfo.FaceId = num3;
										featureInfo.FValid = 1;
										featureInfo.RegisImageName = mainInfo.ShowImageName;
										int num5 = this.featureInfoService.Insert(featureInfo);
										bool flag7 = num5 <= 0;
										if (flag7)
										{
											this.mainInfoService.Delete(num3);
										}
									}
								}
								catch (Exception se3)
								{
									LogHelper.LogError(this.GetType(), se3);
								}
								goto IL_21C;
								IL_248:
								i++;
								continue;
								IL_21C:
								ControlUtil.LabelTextUpdate(this.lbl, string.Format("批量注册中：{0}/{1}", num++, num2));
								goto IL_248;
							}
							this.Invoke(new Action(() =>this.btn_import_Click(null,null)));
						}));
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void enabledControls(bool state)
		{
			this.btn_select.Enabled = state;
			this.btn_import.Enabled = state;
			this.pictureBox1.Visible = !state;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.label1 = new Label();
			this.txt_path = new TextBox();
			this.btn_select = new Button();
			this.lbl = new Label();
			this.btn_import = new Button();
			this.pictureBox1 = new PictureBox();
			((ISupportInitialize)this.pictureBox1).BeginInit();
			base.SuspendLayout();
			this.label1.AutoSize = true;
			this.label1.Location = new Point(22, 23);
			this.label1.Name = "label1";
			this.label1.Size = new Size(53, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "图片目录";
			this.txt_path.Location = new Point(81, 20);
			this.txt_path.Name = "txt_path";
			this.txt_path.Size = new Size(222, 21);
			this.txt_path.TabIndex = 1;
			this.btn_select.Location = new Point(308, 18);
			this.btn_select.Name = "btn_select";
			this.btn_select.Size = new Size(51, 23);
			this.btn_select.TabIndex = 2;
			this.btn_select.Text = "浏览";
			this.btn_select.UseVisualStyleBackColor = true;
			this.btn_select.Click += new EventHandler(this.btn_select_Click);
			this.lbl.AutoSize = true;
			this.lbl.Location = new Point(24, 62);
			this.lbl.Name = "lbl";
			this.lbl.Size = new Size(0, 12);
			this.lbl.TabIndex = 3;
			this.btn_import.Location = new Point(144, 148);
			this.btn_import.Name = "btn_import";
			this.btn_import.Size = new Size(82, 31);
			this.btn_import.TabIndex = 4;
			this.btn_import.Text = "开始导入";
			this.btn_import.UseVisualStyleBackColor = true;
			this.btn_import.Click += new EventHandler(this.btn_import_Click);
			//this.pictureBox1.Image = Resources._5_130H2191324_50;
			this.pictureBox1.Location = new Point(116, 112);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Size(139, 16);
			this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
			this.pictureBox1.TabIndex = 5;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Visible = false;
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(362, 203);
			base.Controls.Add(this.pictureBox1);
			base.Controls.Add(this.btn_import);
			base.Controls.Add(this.lbl);
			base.Controls.Add(this.btn_select);
			base.Controls.Add(this.txt_path);
			base.Controls.Add(this.label1);
			base.Name = "BatchImportForm";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "批量导入";
			((ISupportInitialize)this.pictureBox1).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
