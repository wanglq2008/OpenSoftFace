using OpenSoftFace.Setting.Utils;
using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using CCWin.SkinClass;
using CCWin.SkinControl;
using CommonConstant;
using DAL.Service;
using DAL.Service.Impl;
using LogUtil;
using Model.Entity;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
    public class PersonAddForm : DragBaseForm
    {
        private IMainInfoService mainInfoService;

        private IFeatureInfoService featureInfoService;

        private int personId;

        private FeatureInfo featureInfo;

        private string showDir = Application.StartupPath + "\\OpenSoftFaceFile\\show\\";

        private string tempDir = Application.StartupPath + "\\OpenSoftFaceFile\\temp\\";

        private Image cutImage = null;

        private bool isUpdateImage = false;

        private string regisImageUrl = string.Empty;

        private IniHelper iniHelper = new IniHelper("setting.ini", "setting");

        private MainInfo oldMainInfo = null;

        private string savePath = string.Empty;

        private IContainer components = null;

        private Label lbl_name;

        private Label lbl_num;

        private SkinTextBox txt_num;

        private MyButton btn_cancel;

        private MyButton btn_save;

        private SkinTextBox txt_name;

        private Label lbl_role;

        private SkinTextBox txt_dept;

        private Label lbl_dept;

        private SkinPictureBox pic_image;

        private MyButton btn_image;

        private Label lbl_tip;

        private Label label3;

        private Label label2;

        private Label label1;

        private Panel panel1;

        public PersonAddForm(string save_path)
        {
            this.InitializeComponent();
            this.savePath = save_path;
            this.initDirPath();
            this.Opacity = 0.96;
        }

        public PersonAddForm(int personId, string save_path)
        {
            this.InitializeComponent();
            this.personId = personId;
            this.savePath = save_path;
            this.initDirPath();
            this.Opacity = 0.96;
        }

        private void initDirPath()
        {
            try
            {
                string str = Application.StartupPath + "\\OpenSoftFaceFile\\";
                /*bool flag = !string.IsNullOrWhiteSpace(this.savePath);
                if (flag)
                {
                    bool flag2 = !Directory.Exists(this.savePath);
                    if (flag2)
                    {
                        Directory.CreateDirectory(this.savePath);
                    }
                    str = this.savePath + "\\";
                }*/
                this.showDir = str + "show\\";
                this.tempDir = str + "temp\\";
                bool flag3 = !Directory.Exists(this.showDir);
                if (flag3)
                {
                    Directory.CreateDirectory(this.showDir);
                }
                bool flag4 = !Directory.Exists(this.tempDir);
                if (flag4)
                {
                    Directory.CreateDirectory(this.tempDir);
                }
            }
            catch(Exception err)
            {
                err = err;
            }
        }

        private void AdminManageAddForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.mainInfoService = new MainInfoServiceImpl();
                this.featureInfoService = new FeatureInfoServiceImpl();
                bool flag = this.personId > 0;
                if (flag)
                {
                    this.oldMainInfo = this.mainInfoService.Get(this.personId);
                    bool flag2 = this.oldMainInfo != null;
                    if (flag2)
                    {
                        this.txt_name.Text = this.oldMainInfo.PersonName;
                        this.txt_num.Text = this.oldMainInfo.PersonSerial;
                        this.txt_dept.Text = this.oldMainInfo.Dept;
                        this.pic_image.ImageLocation = this.showDir + this.oldMainInfo.ShowImageName;
                        this.pic_image.SizeMode = PictureBoxSizeMode.Zoom;
                    }
                }
                this.txt_num.SkinTxt.TextChanged += new EventHandler(this.password_text_TextChange);
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

        private void SkinTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool flag = this.personId > 0;
            if (flag)
            {
                e.Handled = true;
            }
        }

        private void password_text_TextChange(object sender, EventArgs e)
        {
            try
            {
                string pattern = "[^a-zA-Z0-9]";
                string value = Regex.Match(this.txt_num.Text, pattern).Value;
                bool flag = !string.IsNullOrEmpty(value);
                if (flag)
                {
                    this.txt_num.Text = this.txt_num.Text.Replace(value, string.Empty);
                }
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string text = this.txt_name.Text.Trim();
                string text2 = this.txt_num.Text.Trim();
                string dept = this.txt_dept.Text.Trim();
                bool flag = string.IsNullOrEmpty(text);
                if (flag)
                {
                    ErrorDialogForm.ShowErrorMsg("姓名不能为空!", "", false, null, false);
                    return;
                }
                bool flag2 = string.IsNullOrEmpty(text2);
                if (flag2)
                {
                    ErrorDialogForm.ShowErrorMsg("编号不能为空!", "", false, null, false);
                    return;
                }
                bool flag3 = this.pic_image == null || this.pic_image.Image == null;
                if (flag3)
                {
                    ErrorDialogForm.ShowErrorMsg("注册照为空或无效!", "", false, null, false);
                    return;
                }

                            int num = 0;
                            try
                            {
                                bool flag4 = this.personId > 0;
                                if (flag4)
                                {
                                    bool flag5 = this.oldMainInfo == null;
                                    if (flag5)
                                    {
                                        ErrorDialogForm.ShowErrorMsg("未查询到对应人员信息!", "", false, null, false);
                                        return;
                                    }
                                    MainInfo mainInfo = this.mainInfoService.Get(text2);
                                    bool flag6 = mainInfo != null && mainInfo.ID != this.oldMainInfo.ID;
                                    if (flag6)
                                    {
                                        ErrorDialogForm.ShowErrorMsg("编号与已有记录冲突，请确认后重试!", "", false, null, false);
                                        return;
                                    }
                                    this.oldMainInfo.PersonName = text;
                                    this.oldMainInfo.PersonSerial = text2;
                                    this.oldMainInfo.Dept = dept;
                                    bool flag7 = this.isUpdateImage;
                                    if (flag7)
                                    {
                                        bool flag8 = this.pic_image.Tag == null;
                                        if (flag8)
                                        {
                                            ErrorDialogForm.ShowErrorMsg("注册照为空或无效!", "", false, null, false);
                                            return;
                                        }
                                        if (File.Exists(this.showDir + this.oldMainInfo.ShowImageName))
                                        {
                                             File.Delete(this.showDir + this.oldMainInfo.ShowImageName); 
                                        }
                                        this.oldMainInfo.ShowImageName = string.Format("show_{0}_{1}.jpg", this.oldMainInfo.ID, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                                        try
                                        {
                                            this.pic_image.Image.Save(this.showDir + this.oldMainInfo.ShowImageName, ImageFormat.Jpeg);
                                        }
                                        catch (Exception se)
                                        {
                                            LogHelper.LogError(base.GetType(), se);
                                        }
                                        bool flag9 = this.featureInfo != null;
                                        if (flag9)
                                        {
                                            this.oldMainInfo.PTop = this.featureInfo.FTop;
                                            this.oldMainInfo.PLeft = this.featureInfo.FLeft;
                                            this.oldMainInfo.PRight = this.featureInfo.FRight;
                                            this.oldMainInfo.PBottom = this.featureInfo.FBottom;
                                            this.oldMainInfo.POrient = this.featureInfo.FOrient;
                                        }
                                        this.featureInfo.FaceId = this.oldMainInfo.ID;
                                        this.featureInfo.PersonId = this.oldMainInfo.ID;
                                        this.featureInfo.RegisImageName = this.oldMainInfo.ShowImageName;
                                        FeatureInfo featureInfo = this.featureInfoService.Get(this.oldMainInfo.ID);
                                        this.featureInfo.FValid = 1;
                                        bool flag10 = featureInfo == null;
                                        int num2;
                                        if (flag10)
                                        {
                                            num2 = this.featureInfoService.Insert(this.featureInfo);
                                        }
                                        else
                                        {
                                            this.featureInfo.ID = featureInfo.ID;
                                            num2 = this.featureInfoService.Update(this.featureInfo);
                                        }
                                        bool flag11 = num2 <= 0;
                                        if (flag11)
                                        {
                                            ErrorDialogForm.ShowErrorMsg("注册照保存失败!", "", false, null, false);
                                            return;
                                        }
                                    }
                                    this.oldMainInfo.PValid = 1;
                                    num = this.mainInfoService.Update(this.oldMainInfo);
                                }
                                else
                                {
                                    bool flag12 = this.featureInfo == null || this.pic_image.Tag == null;
                                    if (flag12)
                                    {
                                        ErrorDialogForm.ShowErrorMsg("注册照为空或无效!", "", false, null, false);
                                        return;
                                    }
                                    MainInfo mainInfo2 = this.mainInfoService.Get(text2);
                                    bool flag13 = mainInfo2 != null;
                                    if (flag13)
                                    {
                                        ErrorDialogForm.ShowErrorMsg("编号与已有记录冲突，请确认后重试!", "", false, null, false);
                                        return;
                                    }
                                    mainInfo2 = new MainInfo();
                                    int num3 = this.mainInfoService.MaxId();
                                    mainInfo2.PersonName = text;
                                    mainInfo2.PersonSerial = text2;
                                    mainInfo2.Dept = dept;
                                    mainInfo2.ID = num3;
                                    mainInfo2.ShowImageName = string.Format("show_{0}_{1}.jpg", num3, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                                    try
                                    {
                                        this.pic_image.Image.Save(this.showDir + "\\" + mainInfo2.ShowImageName, ImageFormat.Jpeg);
                                    }
                                    catch (Exception se2)
                                    {
                                        LogHelper.LogError(base.GetType(), se2);
                                    }
                                    bool flag14 = this.featureInfo != null;
                                    if (flag14)
                                    {
                                        mainInfo2.PTop = this.featureInfo.FTop;
                                        mainInfo2.PLeft = this.featureInfo.FLeft;
                                        mainInfo2.PRight = this.featureInfo.FRight;
                                        mainInfo2.PBottom = this.featureInfo.FBottom;
                                        mainInfo2.POrient = this.featureInfo.FOrient;
                                    }
                                    mainInfo2.PValid = 1;
                                    int num4 = this.mainInfoService.Insert(mainInfo2);
                                    bool flag15 = num4 > 0;
                                    if (flag15)
                                    {
                                        this.featureInfo.PersonId = num3;
                                        this.featureInfo.FaceId = num3;
                                        this.featureInfo.FValid = 1;
                                        this.featureInfo.RegisImageName = mainInfo2.ShowImageName;
                                        int num5 = this.featureInfoService.Insert(this.featureInfo);
                                        bool flag16 = num5 > 0;
                                        if (flag16)
                                        {
                                            num = 1;
                                        }
                                        else
                                        {
                                            this.mainInfoService.Delete(num3);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            bool flag17 = num > 0;
                            if (flag17)
                            {
                                ErrorDialogForm.ShowErrorMsg("保存成功!", "", true, null, false);
                                Program.faceForm.btnClearFaceList_Click(null,null);
                                Program.faceForm.ReloadFace();
                                base.Close();
                            }
                            else
                            {
                                ErrorDialogForm.ShowErrorMsg("保存失败!", "", false, null, false);
                            }
            }
            catch (Exception se3)
            {
                LogHelper.LogError(base.GetType(), se3);
            }
        }

        private void return_btn_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void AdminManageAddForm_Shown(object sender, EventArgs e)
        {
            try
            {
                this.txt_name.Focus();
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

        private void btn_image_Click(object sender, EventArgs e)
        {
            try
            {
                this.picAddImage();
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

        private void picAddImage()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = false;
                openFileDialog.Title = "请选择图片";
                openFileDialog.Filter = "图片文件(*.jpg,*.png,*.bmp)|*.jpg;*.png;*.bmp";
                bool flag = openFileDialog.ShowDialog() == DialogResult.OK;
                if (flag)
                {
                    string fileName = openFileDialog.FileName;
                    float num = 5242880f;
                    float num2 = 2097152f;
                    FileInfo fileInfo = new FileInfo(fileName);
                    bool flag2 = (float)fileInfo.Length > num2;
                    if (flag2)
                    {
                        using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
                        {
                            byte[] bytes = Utils.ImageUtil.CompressionImage(fileStream, 50L, false);
                            bool flag3 = !Directory.Exists(this.tempDir);
                            if (flag3)
                            {
                                Directory.CreateDirectory(this.tempDir);
                            }
                            Random random = new Random();
                            string text = this.tempDir + random.Next(1000, 9999) + ".jpg";
                            File.WriteAllBytes(text, bytes);
                            openFileDialog.FileName = text;
                            fileInfo = new FileInfo(text);
                            fileStream.Dispose();
                            fileStream.Close();
                        }
                    }
                    bool flag4 = (float)fileInfo.Length > num;
                    if (flag4)
                    {
                        ErrorDialogForm.ShowErrorMsg("图片文件大小不得超过5MB!", "", false, null, false);
                    }
                    else
                    {
                        string safeFileName = openFileDialog.SafeFileName;
                        string empty = string.Empty;
                        FeatureInfo featureInfo = SDKOperate.DetectAndRecognitionImage(openFileDialog.FileName, ref empty, ref this.cutImage);
                        bool flag5 = !string.IsNullOrEmpty(empty);
                        if (flag5)
                        {
                            ErrorDialogForm.ShowErrorMsg(empty, "", false, null, false);
                        }
                        else
                        {
                            bool flag6 = this.featureInfo == null;
                            if (flag6)
                            {
                                this.featureInfo = new FeatureInfo();
                            }
                            this.featureInfo.FLeft = featureInfo.FLeft;
                            this.featureInfo.FTop = featureInfo.FTop;
                            this.featureInfo.FRight = featureInfo.FRight;
                            this.featureInfo.FBottom = featureInfo.FBottom;
                            this.featureInfo.FOrient = featureInfo.FOrient;
                            this.featureInfo.RegisImageName = openFileDialog.FileName;
                            this.featureInfo.Feature = featureInfo.Feature;
                            this.featureInfo.FeatureSize = featureInfo.FeatureSize;
                            this.isUpdateImage = true;
                            this.pic_image.Image = this.cutImage;
                            this.pic_image.Tag = openFileDialog.FileName;
                            this.pic_image.SizeMode = PictureBoxSizeMode.Zoom;
                        }
                    }
                }
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

        private void pic_image_Click(object sender, EventArgs e)
        {
            try
            {
                this.picAddImage();
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

        protected override void Dispose(bool disposing)
        {
            bool flag = disposing && this.components != null;
            if (flag)
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonAddForm));
            this.lbl_name = new System.Windows.Forms.Label();
            this.txt_name = new CCWin.SkinControl.SkinTextBox();
            this.btn_save = new OpenSoftFace.Setting.Controls.MyButton();
            this.btn_cancel = new OpenSoftFace.Setting.Controls.MyButton();
            this.lbl_num = new System.Windows.Forms.Label();
            this.txt_num = new CCWin.SkinControl.SkinTextBox();
            this.lbl_role = new System.Windows.Forms.Label();
            this.lbl_dept = new System.Windows.Forms.Label();
            this.txt_dept = new CCWin.SkinControl.SkinTextBox();
            this.pic_image = new CCWin.SkinControl.SkinPictureBox();
            this.btn_image = new OpenSoftFace.Setting.Controls.MyButton();
            this.lbl_tip = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_image)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.Color.White;
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer.Panel2.Controls.Add(this.panel1);
            this.splitContainer.Panel2.Controls.Add(this.label3);
            this.splitContainer.Panel2.Controls.Add(this.label2);
            this.splitContainer.Panel2.Controls.Add(this.label1);
            this.splitContainer.Panel2.Controls.Add(this.pic_image);
            this.splitContainer.Panel2.Controls.Add(this.btn_cancel);
            this.splitContainer.Panel2.Controls.Add(this.btn_image);
            this.splitContainer.Panel2.Controls.Add(this.btn_save);
            this.splitContainer.Panel2.Controls.Add(this.txt_name);
            this.splitContainer.Panel2.Controls.Add(this.lbl_role);
            this.splitContainer.Panel2.Controls.Add(this.txt_dept);
            this.splitContainer.Panel2.Controls.Add(this.lbl_dept);
            this.splitContainer.Panel2.Controls.Add(this.txt_num);
            this.splitContainer.Panel2.Controls.Add(this.lbl_num);
            this.splitContainer.Panel2.Controls.Add(this.lbl_name);
            this.splitContainer.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer_Panel2_Paint);
            this.splitContainer.Size = new System.Drawing.Size(466, 408);
            this.splitContainer.SplitterDistance = 54;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(441, 10);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_title.Size = new System.Drawing.Size(466, 54);
            this.lbl_title.Text = "员工信息";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_name.Location = new System.Drawing.Point(103, 15);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(32, 17);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "姓名";
            this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_name
            // 
            this.txt_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_name.BackColor = System.Drawing.Color.Transparent;
            this.txt_name.DownBack = null;
            this.txt_name.Icon = null;
            this.txt_name.IconIsButton = false;
            this.txt_name.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_name.IsPasswordChat = '\0';
            this.txt_name.IsSystemPasswordChar = false;
            this.txt_name.Lines = new string[0];
            this.txt_name.Location = new System.Drawing.Point(148, 8);
            this.txt_name.Margin = new System.Windows.Forms.Padding(0);
            this.txt_name.MaxLength = 10;
            this.txt_name.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_name.MouseBack = null;
            this.txt_name.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_name.Multiline = false;
            this.txt_name.Name = "txt_name";
            this.txt_name.NormlBack = null;
            this.txt_name.Padding = new System.Windows.Forms.Padding(4);
            this.txt_name.ReadOnly = false;
            this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_name.Size = new System.Drawing.Size(201, 28);
            // 
            // 
            // 
            this.txt_name.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_name.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_name.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_name.SkinTxt.Location = new System.Drawing.Point(4, 4);
            this.txt_name.SkinTxt.MaxLength = 10;
            this.txt_name.SkinTxt.Name = "BaseText";
            this.txt_name.SkinTxt.Size = new System.Drawing.Size(193, 18);
            this.txt_name.SkinTxt.TabIndex = 0;
            this.txt_name.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_name.SkinTxt.WaterText = "姓名不能为空";
            this.txt_name.TabIndex = 0;
            this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_name.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_name.WaterText = "姓名不能为空";
            this.txt_name.WordWrap = true;
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_save.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_save.EnterForeColor = System.Drawing.Color.White;
            this.btn_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_save.FlatAppearance.BorderSize = 0;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.ForeColor = System.Drawing.Color.White;
            this.btn_save.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_save.LeaveForeColor = System.Drawing.Color.White;
            this.btn_save.Location = new System.Drawing.Point(151, 315);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(86, 31);
            this.btn_save.TabIndex = 4;
            this.btn_save.Text = "保存";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.ok_btn_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.White;
            this.btn_cancel.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_cancel.EnterForeColor = System.Drawing.Color.Black;
            this.btn_cancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_cancel.ForeColor = System.Drawing.Color.Black;
            this.btn_cancel.LeaveBackColor = System.Drawing.Color.White;
            this.btn_cancel.LeaveForeColor = System.Drawing.Color.Black;
            this.btn_cancel.Location = new System.Drawing.Point(252, 315);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(86, 31);
            this.btn_cancel.TabIndex = 5;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.return_btn_Click);
            // 
            // lbl_num
            // 
            this.lbl_num.AutoSize = true;
            this.lbl_num.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_num.Location = new System.Drawing.Point(103, 50);
            this.lbl_num.Name = "lbl_num";
            this.lbl_num.Size = new System.Drawing.Size(32, 17);
            this.lbl_num.TabIndex = 1;
            this.lbl_num.Text = "工号";
            this.lbl_num.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_num
            // 
            this.txt_num.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_num.BackColor = System.Drawing.Color.Transparent;
            this.txt_num.DownBack = null;
            this.txt_num.Icon = null;
            this.txt_num.IconIsButton = false;
            this.txt_num.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_num.IsPasswordChat = '\0';
            this.txt_num.IsSystemPasswordChar = false;
            this.txt_num.Lines = new string[0];
            this.txt_num.Location = new System.Drawing.Point(148, 43);
            this.txt_num.Margin = new System.Windows.Forms.Padding(0);
            this.txt_num.MaxLength = 10;
            this.txt_num.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_num.MouseBack = null;
            this.txt_num.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_num.Multiline = false;
            this.txt_num.Name = "txt_num";
            this.txt_num.NormlBack = null;
            this.txt_num.Padding = new System.Windows.Forms.Padding(4);
            this.txt_num.ReadOnly = false;
            this.txt_num.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_num.Size = new System.Drawing.Size(201, 28);
            // 
            // 
            // 
            this.txt_num.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_num.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_num.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_num.SkinTxt.Location = new System.Drawing.Point(4, 4);
            this.txt_num.SkinTxt.MaxLength = 10;
            this.txt_num.SkinTxt.Name = "BaseText";
            this.txt_num.SkinTxt.Size = new System.Drawing.Size(193, 18);
            this.txt_num.SkinTxt.TabIndex = 0;
            this.txt_num.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_num.SkinTxt.WaterText = "编号不能为空,仅支持数字或字母";
            this.txt_num.TabIndex = 1;
            this.txt_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_num.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_num.WaterText = "编号不能为空";
            this.txt_num.WordWrap = true;
            // 
            // lbl_role
            // 
            this.lbl_role.AutoSize = true;
            this.lbl_role.Location = new System.Drawing.Point(103, 179);
            this.lbl_role.Name = "lbl_role";
            this.lbl_role.Size = new System.Drawing.Size(32, 17);
            this.lbl_role.TabIndex = 10;
            this.lbl_role.Text = "照片";
            this.lbl_role.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_dept
            // 
            this.lbl_dept.AutoSize = true;
            this.lbl_dept.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_dept.Location = new System.Drawing.Point(103, 85);
            this.lbl_dept.Name = "lbl_dept";
            this.lbl_dept.Size = new System.Drawing.Size(32, 17);
            this.lbl_dept.TabIndex = 1;
            this.lbl_dept.Text = "部门";
            this.lbl_dept.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_dept
            // 
            this.txt_dept.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_dept.BackColor = System.Drawing.Color.Transparent;
            this.txt_dept.DownBack = null;
            this.txt_dept.Icon = null;
            this.txt_dept.IconIsButton = false;
            this.txt_dept.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_dept.IsPasswordChat = '\0';
            this.txt_dept.IsSystemPasswordChar = false;
            this.txt_dept.Lines = new string[0];
            this.txt_dept.Location = new System.Drawing.Point(148, 78);
            this.txt_dept.Margin = new System.Windows.Forms.Padding(0);
            this.txt_dept.MaxLength = 10;
            this.txt_dept.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_dept.MouseBack = null;
            this.txt_dept.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_dept.Multiline = false;
            this.txt_dept.Name = "txt_dept";
            this.txt_dept.NormlBack = null;
            this.txt_dept.Padding = new System.Windows.Forms.Padding(4);
            this.txt_dept.ReadOnly = false;
            this.txt_dept.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_dept.Size = new System.Drawing.Size(201, 28);
            // 
            // 
            // 
            this.txt_dept.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_dept.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_dept.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_dept.SkinTxt.Location = new System.Drawing.Point(4, 4);
            this.txt_dept.SkinTxt.MaxLength = 10;
            this.txt_dept.SkinTxt.Name = "BaseText";
            this.txt_dept.SkinTxt.Size = new System.Drawing.Size(193, 18);
            this.txt_dept.SkinTxt.TabIndex = 0;
            this.txt_dept.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_dept.SkinTxt.WaterText = "";
            this.txt_dept.TabIndex = 2;
            this.txt_dept.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_dept.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_dept.WaterText = "";
            this.txt_dept.WordWrap = true;
            // 
            // pic_image
            // 
            this.pic_image.BackColor = System.Drawing.SystemColors.Control;
            this.pic_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pic_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_image.ImageLocation = "";
            this.pic_image.Location = new System.Drawing.Point(149, 113);
            this.pic_image.Name = "pic_image";
            this.pic_image.Size = new System.Drawing.Size(198, 170);
            this.pic_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_image.TabIndex = 11;
            this.pic_image.TabStop = false;
            // 
            // btn_image
            // 
            this.btn_image.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_image.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_image.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(235)))), ((int)(((byte)(254)))));
            this.btn_image.EnterForeColor = System.Drawing.Color.White;
            this.btn_image.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_image.FlatAppearance.BorderSize = 0;
            this.btn_image.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_image.ForeColor = System.Drawing.Color.White;
            this.btn_image.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_image.LeaveForeColor = System.Drawing.Color.White;
            this.btn_image.Location = new System.Drawing.Point(212, 285);
            this.btn_image.Name = "btn_image";
            this.btn_image.Size = new System.Drawing.Size(66, 26);
            this.btn_image.TabIndex = 4;
            this.btn_image.Text = "上传照片";
            this.btn_image.UseVisualStyleBackColor = false;
            this.btn_image.Click += new System.EventHandler(this.btn_image_Click);
            // 
            // lbl_tip
            // 
            this.lbl_tip.AutoSize = true;
            this.lbl_tip.Location = new System.Drawing.Point(17, 4);
            this.lbl_tip.Name = "lbl_tip";
            this.lbl_tip.Size = new System.Drawing.Size(368, 68);
            this.lbl_tip.TabIndex = 10;
            this.lbl_tip.Text = "不推荐的照片：含多个人脸的照片\r\n                     脸部被遮挡的照片（墨镜、口罩、手势等）\r\n                     人" +
    "脸模糊的照片\r\n                     人脸表情扭曲，或姿势不端正的照片                     \r\n";
            this.lbl_tip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(93, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(93, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(93, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "*";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.lbl_tip);
            this.panel1.Location = new System.Drawing.Point(387, 156);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 80);
            this.panel1.TabIndex = 13;
            this.panel1.Visible = false;
            // 
            // PersonAddForm
            // 
            this.ClientSize = new System.Drawing.Size(466, 408);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "PersonAddForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AdminManageAddForm_Load);
            this.Shown += new System.EventHandler(this.AdminManageAddForm_Shown);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_image)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void splitContainer_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
