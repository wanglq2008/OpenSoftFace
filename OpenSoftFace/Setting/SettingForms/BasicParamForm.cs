using OpenSoftFace.Setting.Utils;
using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using CommonConstant;
using LogUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Web;
using System.IO;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
	public class BasicParamForm : Form
	{
		private IniHelper iniHelper = new IniHelper("setting.ini", "setting");

		private int restartState = 0;

		private BaseForm baseForm;

		private IContainer components = null;

		private MyButton btn_update;

		private MyTextBox txt_recog_score;

		//private RadioButton rab_recog_mode1;

		//private RadioButton rab_recog_mode2;

		private GroupBox gbx_basic;

		private Label lbl_recog_score;

		//private Label lbl_other_setting;

		//private Label lbl_recog_retry;

		//private Label lbl_distance;

		//private Label lbl_recog_mode;

		//private RadioButton rab_distance1;

		//private RadioButton rab_distance2;

		//private RadioButton rab_distance3;

		//private Panel panel_liveness;

		//private Panel panel_distance;

		private Label lbl_recog_score_tip;

		private Label lbl_active_status_value;

		private Label lbl_active_status;

		//private CheckBox cbx_top;

		//private CheckBox cbx_preview;

		//private Label lbl_recog_retry_tip;

		//private MySwitch switch_recog_retry;

		//private GroupBox D;

		private MyComboBox cbx_pre_voice;

		private Label lbl_text_control;

		private RadioButton rab_sub_name;

		private RadioButton rab_name;

		private Label lbl_sub_name_tip;

		private Panel panel_show;

		private MySwitch switch_show_text;

		private Label lbl_show_text;

		private Label lbl_voice;

		private MySwitch switch_show_voice;

		private Label lbl_show_voice;

		private TableLayoutPanel tableLayoutPanel1;

		private Label lbl_effective_date_value;

		private Label lbl_effective_date;

		private Label lbl_effective_date_tip;
        private Label label2;
        private Label label3;
        private MyTextBox txt_APPID;
        private MyTextBox txt_SDKKEY;
        private CCWin.SkinControl.SkinPictureBox pic_image_logo;
        private Label lbl_role;
        private Label lbl_dept;
        private MyButton btn_image;

		private Label label1;
        private MyTextBox txt_company_info;
        private Label label4;
        private MyTextBox txt_company_name;

        private string tempDir = Application.StartupPath + "\\OpenSoftFaceFile\\temp\\";

		//private MyTextBox txt_save_path;

		//private Label label8;

		//private MySwitch switch_liveness_control;

		//private Label lbl_liveness_control;

		//private Label lbl_liveness_value_tip;

		//private MyTextBox txt_liveness_value;

		//private Label lbl_liveness_value;

		//private Label lbl_preview_resolution;

		//private MyComboBox cbx_preview_resolution;

		public BasicParamForm(BaseForm baseForm)
		{
			this.InitializeComponent();
			base.TopMost = true;
			this.baseForm = baseForm;
		}

		private void BasicParamForm_Shown(object sender, EventArgs e)
		{
			try
			{
				Dictionary<string, string> source = new Dictionary<string, string>
				{
					{
						"识别成功",
						"1"
					},
					{
						"打卡成功",
						"2"
					}
				};
				this.initCombox(source, this.cbx_pre_voice);
				this.fillInfoControl();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void fillInfoControl()
		{
			try
			{
                this.txt_APPID.Text = this.iniHelper.Dict["APPID"];
                this.txt_SDKKEY.Text = this.iniHelper.Dict["SDKKEY"];

                this.txt_company_info.Text = HttpUtility.UrlDecode(this.iniHelper.Dict["COMPANY_INFO"]);

                this.txt_company_name.Text = HttpUtility.UrlDecode(this.iniHelper.Dict["COMPANY_NAME"]);


                this.pic_image_logo.ImageLocation = HttpUtility.UrlDecode(this.iniHelper.Dict["IMAGE_LOGO"]);

				this.lbl_active_status_value.Text = (SDKOperate.ActiveStatus ? "已激活" : "未激活");
				bool flag = !SDKOperate.ActiveStatus;
				if (flag)
				{
					this.lbl_active_status_value.ForeColor = Color.Red;
				}
				else
				{
					//this.lbl_effective_date_tip.Text = (string.IsNullOrEmpty(SDKOperate.SDKVersion) ? string.Empty : string.Format("ArcFace {0}", SDKOperate.SDKVersion));
				}
				this.lbl_effective_date_value.Text = SDKOperate.DeadlineDate;
				bool flag2 = this.iniHelper.Dict.ContainsKey("thresh");
				if (flag2)
				{
					this.txt_recog_score.Text = this.iniHelper.Dict["thresh"];
				}
				bool flag3 = false;
				bool flag4 = this.iniHelper.Dict.ContainsKey("liveness_control") && SwitchStateConst.TRUE_VALUE.Equals(this.iniHelper.Dict["liveness_control"]);
				if (flag4)
				{
					flag3 = true;
				}
				//this.switch_liveness_control.SwitchStatus = flag3;
				//this.txt_liveness_value.Enabled = flag3;
				bool flag5 = this.iniHelper.Dict.ContainsKey("liveness_threshold");
				if (flag5)
				{
					//this.txt_liveness_value.Text = this.iniHelper.Dict["liveness_threshold"];
				}
				string value = PreviewResolution.VIEW_640_480;
				bool flag6 = this.iniHelper.Dict.ContainsKey("preview_resolution");
				if (flag6)
				{
					value = this.iniHelper.Dict["preview_resolution"];
				}
				bool flag7 = PreviewResolution.VIEW_1280_720.Equals(value);
				if (flag7)
				{
					//this.cbx_preview_resolution.SelectedIndex = 1;
				}
				else
				{
					//this.cbx_preview_resolution.SelectedIndex = 0;
				}
				bool switchStatus = false;
				bool flag8 = this.iniHelper.Dict.ContainsKey("fr_control") && SwitchStateConst.TRUE_VALUE.Equals(this.iniHelper.Dict["fr_control"]);
				if (flag8)
				{
					switchStatus = true;
				}
				//this.switch_recog_retry.SwitchStatus = switchStatus;
				string text = "10";
				bool flag9 = this.iniHelper.Dict.ContainsKey("face_nscale");
				if (flag9)
				{
					text = this.iniHelper.Dict["face_nscale"];
				}
				string a = text;
				if (!(a == "8"))
				{
					if (!(a == "16"))
					{
						//this.rab_distance2.Checked = true;
					}
					else
					{
						//this.rab_distance3.Checked = true;
					}
				}
				else
				{
					//this.rab_distance1.Checked = true;
				}
				//this.rab_recog_mode1.Checked = true;
				//bool flag10 = this.iniHelper.Dict.ContainsKey("show_face_mode") && "2".Equals(this.iniHelper.Dict["show_face_mode"]);
				//if (flag10)
				//{
				//	this.rab_recog_mode2.Checked = true;
				//}
				bool @checked = false;
				bool flag11 = this.iniHelper.Dict.ContainsKey("flip") && SwitchStateConst.TRUE_VALUE.Equals(this.iniHelper.Dict["flip"]);
				if (flag11)
				{
					@checked = true;
				}
				//this.cbx_preview.Checked = @checked;
				bool checked2 = false;
				bool flag12 = this.iniHelper.Dict.ContainsKey("top_show") && SwitchStateConst.TRUE_VALUE.Equals(this.iniHelper.Dict["top_show"]);
				if (flag12)
				{
					checked2 = true;
				}
				//this.cbx_top.Checked = checked2;
				string text2 = "1";
				bool flag13 = this.iniHelper.Dict.ContainsKey("show_mask");
				if (flag13)
				{
					text2 = this.iniHelper.Dict["show_mask"];
				}
				string a2 = text2;
				if (!(a2 == "2"))
				{
					if (!(a2 == "0"))
					{
						this.rab_name.Enabled = true;
						this.rab_sub_name.Enabled = true;
						this.switch_show_text.SwitchStatus = true;
						this.rab_name.Checked = true;
					}
					else
					{
						this.rab_name.Checked = true;
						this.switch_show_text.SwitchStatus = false;
						this.rab_name.Enabled = false;
						this.rab_sub_name.Enabled = false;
					}
				}
				else
				{
					this.switch_show_text.SwitchStatus = true;
					this.rab_name.Enabled = true;
					this.rab_sub_name.Enabled = true;
					this.rab_sub_name.Checked = true;
				}
				string text3 = "1";
				bool flag14 = this.iniHelper.Dict.ContainsKey("show_voice");
				if (flag14)
				{
					text3 = this.iniHelper.Dict["show_voice"];
				}
				string a3 = text3;
				if (!(a3 == "0"))
				{
					if (!(a3 == "2"))
					{
						this.switch_show_voice.SwitchStatus = true;
						this.cbx_pre_voice.Enabled = true;
						this.cbx_pre_voice.SelectedIndex = 0;
					}
					else
					{
						this.switch_show_voice.SwitchStatus = true;
						this.cbx_pre_voice.Enabled = true;
						this.cbx_pre_voice.SelectedIndex = 1;
					}
				}
				else
				{
					this.switch_show_voice.SwitchStatus = false;
					this.cbx_pre_voice.SelectedIndex = 0;
				}
				string text4 = string.Empty;
				bool flag15 = this.iniHelper.Dict.ContainsKey("save_path");
				if (flag15)
				{
					text4 = this.iniHelper.Dict["save_path"];
				}
				//this.txt_save_path.Text = text4;
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private string checkValue(string key, string recognitionScore)
		{
			string result = string.Empty;
			bool flag = string.IsNullOrWhiteSpace(recognitionScore);
			if (flag)
			{
				result = string.Format("{0}为0~1的浮点数!", key);
			}
			bool flag2 = !Regex.Match(recognitionScore, "^0(\\.[0-9]{1,3})?$").Success;
			if (flag2)
			{
				double num = 1.0;
				try
				{
					num = Convert.ToDouble(recognitionScore.Trim());
				}
				catch
				{
					num = 0.0;
				}
				bool flag3 = recognitionScore != "1" && num > 1.0;
				if (flag3)
				{
					result = string.Format("{0}为0~1的浮点数!", key);
				}
			}
			return result;
		}

		private void updateBtn_Click(object sender, EventArgs e)
		{
			try
			{
				string text = this.txt_recog_score.Text;
				//string text2 = this.txt_liveness_value.Text;
				string text3 = this.checkValue("识别阈值", text);
				bool flag = !string.IsNullOrEmpty(text3);
                if (flag || string.IsNullOrEmpty(this.txt_APPID.Text) || string.IsNullOrEmpty(this.txt_SDKKEY.Text))
				{
					ErrorDialogForm.ShowErrorMsg(text3, "", false, null, false);
				}
				else
				{
					//text3 = this.checkValue("活体阈值", text2);
					bool flag2 = !string.IsNullOrEmpty(text3);
					if (flag2)
					{
						ErrorDialogForm.ShowErrorMsg(text3, "", false, null, false);
					}
					else
					{
						this.iniHelper.Set("thresh", text);
						//this.iniHelper.Set("liveness_control", this.switch_liveness_control.SwitchStatus ? SwitchStateConst.TRUE_VALUE : SwitchStateConst.FALSE_VALUE);
						//this.iniHelper.Set("liveness_threshold", text2);
						//this.iniHelper.Set("preview_resolution", this.cbx_preview_resolution.SelectedIndex.ToString());
						//this.iniHelper.Set("fr_control", this.switch_recog_retry.SwitchStatus ? SwitchStateConst.TRUE_VALUE : SwitchStateConst.FALSE_VALUE);
						string val = "10";
						//bool @checked = this.rab_distance1.Checked;
						//if (@checked)
						//{
						//	val = "8";
						//}
						//else
						//{
						//	bool checked2 = this.rab_distance3.Checked;
						//	if (checked2)
						//	{
						//		val = "16";
						//	}
						//}
                        this.iniHelper.Set("APPID", this.txt_APPID.Text);
                        this.iniHelper.Set("SDKKEY", this.txt_SDKKEY.Text);

                        this.iniHelper.Set("APPID", this.txt_APPID.Text);
                        string company_info = HttpUtility.UrlEncode(this.txt_company_info.Text);
                        this.iniHelper.Set("COMPANY_INFO", company_info);

                        string company_name = HttpUtility.UrlEncode(this.txt_company_name.Text);
                        this.iniHelper.Set("COMPANY_NAME", company_name);

                        string image_logo = HttpUtility.UrlEncode(this.pic_image_logo.ImageLocation);
                        this.iniHelper.Set("IMAGE_LOGO", image_logo);

						string val2 = "1";
						//bool checked3 = this.rab_recog_mode2.Checked;
						//if (checked3)
						//{
						//	val2 = "2";
						//}
						//this.iniHelper.Set("show_face_mode", val2);
						string val3 = SwitchStateConst.FALSE_VALUE;
						//bool checked4 = this.cbx_preview.Checked;
						//if (checked4)
						//{
						//	val3 = SwitchStateConst.TRUE_VALUE;
						//}
						this.iniHelper.Set("flip", val3);
						string val4 = SwitchStateConst.FALSE_VALUE;
						//bool checked5 = this.cbx_top.Checked;
						//if (checked5)
						//{
						//	val4 = SwitchStateConst.TRUE_VALUE;
						//}
						this.iniHelper.Set("top_show", val4);
						string val5 = "1";
						bool flag3 = !this.switch_show_text.SwitchStatus;
						if (flag3)
						{
							val5 = "0";
						}
						else
						{
							bool checked6 = this.rab_sub_name.Checked;
							if (checked6)
							{
								val5 = "2";
							}
						}
						this.iniHelper.Set("show_mask", val5);
						string val6 = "1";
						bool flag4 = !this.switch_show_voice.SwitchStatus;
						if (flag4)
						{
							val6 = "0";
						}
						else
						{
							bool flag5 = this.cbx_pre_voice.SelectedIndex == 1;
							if (flag5)
							{
								val6 = "2";
							}
						}
						this.iniHelper.Set("show_voice", val6);
						bool flag6 = this.restartState <= 0;
						if (flag6)
						{
							DialogResult dialogResult = ConfirmDialogForm.showWarningMsg();
							bool flag7 = DialogResult.OK.Equals(dialogResult);
							if (flag7)
							{
								this.baseForm.Hide();
								OperatorUtil.RestartExe();
							}
							else
							{
								bool flag8 = DialogResult.Cancel.Equals(dialogResult);
								if (flag8)
								{
									this.restartState++;
								}
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				ErrorDialogForm.ShowErrorMsg("设置失败，请稍后重试!", "", true, this, false);
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void txt_recogScore_KeyPress(object sender, KeyPressEventArgs e)
		{
			try
			{
				bool flag = e.KeyChar == '.' && (this.txt_recog_score.Text.Length == 0 || this.txt_recog_score.SelectionLength == this.txt_recog_score.Text.Length);
				if (flag)
				{
					e.Handled = true;
				}
				bool flag2 = e.KeyChar != '\b' && !char.IsDigit(e.KeyChar) && e.KeyChar != '.';
				if (flag2)
				{
					e.Handled = true;
				}
				bool flag3 = e.KeyChar == '.' && ((TextBox)sender).Text.IndexOf(".") >= 0;
				if (flag3)
				{
					e.Handled = true;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void switch_show_text_Click(object sender, EventArgs e)
		{
			bool switchStatus = this.switch_show_text.SwitchStatus;
			this.rab_name.Enabled = switchStatus;
			this.rab_sub_name.Enabled = switchStatus;
		}

		private void switch_show_voice_Click(object sender, EventArgs e)
		{
			this.cbx_pre_voice.Enabled = this.switch_show_voice.SwitchStatus;
		}

		private void initCombox(Dictionary<string, string> source, ComboBox comBox)
		{
			try
			{
				comBox.DataSource = new BindingSource(source, null);
				comBox.DisplayMember = "Key";
				comBox.ValueMember = "Value";
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void btn_image_Click(object sender, EventArgs e)
        {

        }

		private void switch_liveness_control_Click(object sender, EventArgs e)
		{
			//this.txt_liveness_value.Enabled = this.switch_liveness_control.SwitchStatus;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.gbx_basic = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_effective_date_tip = new System.Windows.Forms.Label();
            this.lbl_recog_score_tip = new System.Windows.Forms.Label();
            this.lbl_effective_date_value = new System.Windows.Forms.Label();
            this.lbl_active_status_value = new System.Windows.Forms.Label();
            this.lbl_effective_date = new System.Windows.Forms.Label();
            this.lbl_active_status = new System.Windows.Forms.Label();
            this.lbl_recog_score = new System.Windows.Forms.Label();
            this.lbl_show_voice = new System.Windows.Forms.Label();
            this.lbl_voice = new System.Windows.Forms.Label();
            this.lbl_show_text = new System.Windows.Forms.Label();
            this.lbl_text_control = new System.Windows.Forms.Label();
            this.panel_show = new System.Windows.Forms.Panel();
            this.rab_sub_name = new System.Windows.Forms.RadioButton();
            this.lbl_sub_name_tip = new System.Windows.Forms.Label();
            this.rab_name = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_update = new OpenSoftFace.Setting.Controls.MyButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pic_image_logo = new CCWin.SkinControl.SkinPictureBox();
            this.lbl_role = new System.Windows.Forms.Label();
            this.lbl_dept = new System.Windows.Forms.Label();
            this.txt_company_info = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.btn_image = new OpenSoftFace.Setting.Controls.MyButton();
            this.txt_SDKKEY = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.txt_APPID = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.txt_recog_score = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.switch_show_voice = new OpenSoftFace.Setting.Controls.MySwitch();
            this.switch_show_text = new OpenSoftFace.Setting.Controls.MySwitch();
            this.cbx_pre_voice = new OpenSoftFace.Setting.Controls.MyComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_company_name = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.panel_show.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_image_logo)).BeginInit();
            this.SuspendLayout();
            // 
            // gbx_basic
            // 
            this.gbx_basic.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gbx_basic.Location = new System.Drawing.Point(13, 8);
            this.gbx_basic.Name = "gbx_basic";
            this.gbx_basic.Size = new System.Drawing.Size(642, 270);
            this.gbx_basic.TabIndex = 49;
            this.gbx_basic.TabStop = false;
            this.gbx_basic.Text = " 基础参数 ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(197, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 17);
            this.label1.TabIndex = 52;
            this.label1.Text = "使用到期后，请重新更新APPID和SDKKEY！";
            // 
            // lbl_effective_date_tip
            // 
            this.lbl_effective_date_tip.AutoSize = true;
            this.lbl_effective_date_tip.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_effective_date_tip.ForeColor = System.Drawing.Color.Gray;
            this.lbl_effective_date_tip.Location = new System.Drawing.Point(153, 294);
            this.lbl_effective_date_tip.Name = "lbl_effective_date_tip";
            this.lbl_effective_date_tip.Size = new System.Drawing.Size(0, 17);
            this.lbl_effective_date_tip.TabIndex = 52;
            // 
            // lbl_recog_score_tip
            // 
            this.lbl_recog_score_tip.AutoSize = true;
            this.lbl_recog_score_tip.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_recog_score_tip.ForeColor = System.Drawing.Color.Gray;
            this.lbl_recog_score_tip.Location = new System.Drawing.Point(229, 161);
            this.lbl_recog_score_tip.Name = "lbl_recog_score_tip";
            this.lbl_recog_score_tip.Size = new System.Drawing.Size(140, 17);
            this.lbl_recog_score_tip.TabIndex = 52;
            this.lbl_recog_score_tip.Text = "建议人脸阈值0.80及以上";
            // 
            // lbl_effective_date_value
            // 
            this.lbl_effective_date_value.AutoSize = true;
            this.lbl_effective_date_value.Location = new System.Drawing.Point(127, 123);
            this.lbl_effective_date_value.Name = "lbl_effective_date_value";
            this.lbl_effective_date_value.Size = new System.Drawing.Size(65, 12);
            this.lbl_effective_date_value.TabIndex = 52;
            this.lbl_effective_date_value.Text = "2020/06/06";
            // 
            // lbl_active_status_value
            // 
            this.lbl_active_status_value.AutoSize = true;
            this.lbl_active_status_value.Location = new System.Drawing.Point(107, 388);
            this.lbl_active_status_value.Name = "lbl_active_status_value";
            this.lbl_active_status_value.Size = new System.Drawing.Size(41, 12);
            this.lbl_active_status_value.TabIndex = 52;
            this.lbl_active_status_value.Text = "已激活";
            this.lbl_active_status_value.Visible = false;
            // 
            // lbl_effective_date
            // 
            this.lbl_effective_date.AutoSize = true;
            this.lbl_effective_date.Location = new System.Drawing.Point(35, 122);
            this.lbl_effective_date.Name = "lbl_effective_date";
            this.lbl_effective_date.Size = new System.Drawing.Size(41, 12);
            this.lbl_effective_date.TabIndex = 52;
            this.lbl_effective_date.Text = "使用期";
            // 
            // lbl_active_status
            // 
            this.lbl_active_status.AutoSize = true;
            this.lbl_active_status.Location = new System.Drawing.Point(16, 388);
            this.lbl_active_status.Name = "lbl_active_status";
            this.lbl_active_status.Size = new System.Drawing.Size(53, 12);
            this.lbl_active_status.TabIndex = 52;
            this.lbl_active_status.Text = "激活状态";
            this.lbl_active_status.Visible = false;
            // 
            // lbl_recog_score
            // 
            this.lbl_recog_score.AutoSize = true;
            this.lbl_recog_score.Location = new System.Drawing.Point(35, 164);
            this.lbl_recog_score.Name = "lbl_recog_score";
            this.lbl_recog_score.Size = new System.Drawing.Size(53, 12);
            this.lbl_recog_score.TabIndex = 52;
            this.lbl_recog_score.Text = "人脸阈值";
            // 
            // lbl_show_voice
            // 
            this.lbl_show_voice.AutoSize = true;
            this.lbl_show_voice.Location = new System.Drawing.Point(13, 80);
            this.lbl_show_voice.Name = "lbl_show_voice";
            this.lbl_show_voice.Size = new System.Drawing.Size(80, 17);
            this.lbl_show_voice.TabIndex = 16;
            this.lbl_show_voice.Text = "弹窗提示语音";
            // 
            // lbl_voice
            // 
            this.lbl_voice.AutoSize = true;
            this.lbl_voice.Location = new System.Drawing.Point(13, 59);
            this.lbl_voice.Name = "lbl_voice";
            this.lbl_voice.Size = new System.Drawing.Size(56, 17);
            this.lbl_voice.TabIndex = 16;
            this.lbl_voice.Text = "播放语音";
            // 
            // lbl_show_text
            // 
            this.lbl_show_text.AutoSize = true;
            this.lbl_show_text.Location = new System.Drawing.Point(13, 38);
            this.lbl_show_text.Name = "lbl_show_text";
            this.lbl_show_text.Size = new System.Drawing.Size(80, 17);
            this.lbl_show_text.TabIndex = 16;
            this.lbl_show_text.Text = "弹窗提示文字";
            // 
            // lbl_text_control
            // 
            this.lbl_text_control.AutoSize = true;
            this.lbl_text_control.Location = new System.Drawing.Point(13, 17);
            this.lbl_text_control.Name = "lbl_text_control";
            this.lbl_text_control.Size = new System.Drawing.Size(56, 17);
            this.lbl_text_control.TabIndex = 16;
            this.lbl_text_control.Text = "弹框显示";
            // 
            // panel_show
            // 
            this.panel_show.Controls.Add(this.rab_sub_name);
            this.panel_show.Controls.Add(this.lbl_sub_name_tip);
            this.panel_show.Controls.Add(this.rab_name);
            this.panel_show.Location = new System.Drawing.Point(101, 33);
            this.panel_show.Name = "panel_show";
            this.panel_show.Size = new System.Drawing.Size(301, 27);
            this.panel_show.TabIndex = 54;
            // 
            // rab_sub_name
            // 
            this.rab_sub_name.AutoSize = true;
            this.rab_sub_name.ForeColor = System.Drawing.Color.Black;
            this.rab_sub_name.Location = new System.Drawing.Point(54, 4);
            this.rab_sub_name.Name = "rab_sub_name";
            this.rab_sub_name.Size = new System.Drawing.Size(47, 16);
            this.rab_sub_name.TabIndex = 16;
            this.rab_sub_name.TabStop = true;
            this.rab_sub_name.Tag = "2";
            this.rab_sub_name.Text = "姓 *";
            this.rab_sub_name.UseVisualStyleBackColor = true;
            // 
            // lbl_sub_name_tip
            // 
            this.lbl_sub_name_tip.AutoSize = true;
            this.lbl_sub_name_tip.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_sub_name_tip.ForeColor = System.Drawing.Color.Gray;
            this.lbl_sub_name_tip.Location = new System.Drawing.Point(99, 6);
            this.lbl_sub_name_tip.Name = "lbl_sub_name_tip";
            this.lbl_sub_name_tip.Size = new System.Drawing.Size(80, 17);
            this.lbl_sub_name_tip.TabIndex = 50;
            this.lbl_sub_name_tip.Text = "隐藏第二个字";
            // 
            // rab_name
            // 
            this.rab_name.AutoSize = true;
            this.rab_name.Location = new System.Drawing.Point(4, 4);
            this.rab_name.Name = "rab_name";
            this.rab_name.Size = new System.Drawing.Size(47, 16);
            this.rab_name.TabIndex = 15;
            this.rab_name.TabStop = true;
            this.rab_name.Tag = "1";
            this.rab_name.Text = "姓名";
            this.rab_name.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btn_update, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(291, 372);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(86, 31);
            this.tableLayoutPanel1.TabIndex = 53;
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_update.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_update.EnterForeColor = System.Drawing.Color.White;
            this.btn_update.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_update.FlatAppearance.BorderSize = 0;
            this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_update.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btn_update.ForeColor = System.Drawing.Color.White;
            this.btn_update.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_update.LeaveForeColor = System.Drawing.Color.White;
            this.btn_update.Location = new System.Drawing.Point(3, 3);
            this.btn_update.MaximumSize = new System.Drawing.Size(86, 31);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(80, 25);
            this.btn_update.TabIndex = 18;
            this.btn_update.Text = "确认";
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 54;
            this.label2.Text = "APPID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 55;
            this.label3.Text = "SDKKEY";
            // 
            // pic_image_logo
            // 
            this.pic_image_logo.BackColor = System.Drawing.Color.Transparent;
            this.pic_image_logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_image_logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_image_logo.ImageLocation = "";
            this.pic_image_logo.Location = new System.Drawing.Point(127, 270);
            this.pic_image_logo.Name = "pic_image_logo";
            this.pic_image_logo.Size = new System.Drawing.Size(230, 80);
            this.pic_image_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_image_logo.TabIndex = 61;
            this.pic_image_logo.TabStop = false;
            // 
            // lbl_role
            // 
            this.lbl_role.AutoSize = true;
            this.lbl_role.Location = new System.Drawing.Point(14, 299);
            this.lbl_role.Name = "lbl_role";
            this.lbl_role.Size = new System.Drawing.Size(101, 12);
            this.lbl_role.TabIndex = 60;
            this.lbl_role.Text = "企业logo(230*80)";
            this.lbl_role.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_dept
            // 
            this.lbl_dept.AutoSize = true;
            this.lbl_dept.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_dept.Location = new System.Drawing.Point(36, 226);
            this.lbl_dept.Name = "lbl_dept";
            this.lbl_dept.Size = new System.Drawing.Size(56, 17);
            this.lbl_dept.TabIndex = 58;
            this.lbl_dept.Text = "企业信息";
            this.lbl_dept.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_company_info
            // 
            this.txt_company_info.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_company_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_company_info.EmptyTextTip = null;
            this.txt_company_info.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_company_info.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_company_info.Location = new System.Drawing.Point(127, 224);
            this.txt_company_info.MaxLength = 50;
            this.txt_company_info.Name = "txt_company_info";
            this.txt_company_info.Size = new System.Drawing.Size(339, 23);
            this.txt_company_info.TabIndex = 63;
            // 
            // btn_image
            // 
            this.btn_image.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_image.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_image.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(235)))), ((int)(((byte)(254)))));
            this.btn_image.EnterForeColor = System.Drawing.Color.White;
            this.btn_image.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_image.FlatAppearance.BorderSize = 0;
            this.btn_image.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_image.ForeColor = System.Drawing.Color.White;
            this.btn_image.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_image.LeaveForeColor = System.Drawing.Color.White;
            this.btn_image.Location = new System.Drawing.Point(363, 299);
            this.btn_image.Name = "btn_image";
            this.btn_image.Size = new System.Drawing.Size(66, 26);
            this.btn_image.TabIndex = 62;
            this.btn_image.Text = "上传照片";
            this.btn_image.UseVisualStyleBackColor = false;
            this.btn_image.Click += new System.EventHandler(this.btn_image_Click_1);
            // 
            // txt_SDKKEY
            // 
            this.txt_SDKKEY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_SDKKEY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SDKKEY.EmptyTextTip = null;
            this.txt_SDKKEY.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_SDKKEY.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_SDKKEY.Location = new System.Drawing.Point(127, 74);
            this.txt_SDKKEY.MaxLength = 50;
            this.txt_SDKKEY.Name = "txt_SDKKEY";
            this.txt_SDKKEY.Size = new System.Drawing.Size(339, 23);
            this.txt_SDKKEY.TabIndex = 57;
            // 
            // txt_APPID
            // 
            this.txt_APPID.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_APPID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_APPID.EmptyTextTip = null;
            this.txt_APPID.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_APPID.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_APPID.Location = new System.Drawing.Point(127, 30);
            this.txt_APPID.MaxLength = 50;
            this.txt_APPID.Name = "txt_APPID";
            this.txt_APPID.Size = new System.Drawing.Size(339, 23);
            this.txt_APPID.TabIndex = 56;
            // 
            // txt_recog_score
            // 
            this.txt_recog_score.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_recog_score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_recog_score.EmptyTextTip = null;
            this.txt_recog_score.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_recog_score.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_recog_score.Location = new System.Drawing.Point(127, 158);
            this.txt_recog_score.MaxLength = 4;
            this.txt_recog_score.Name = "txt_recog_score";
            this.txt_recog_score.Size = new System.Drawing.Size(100, 23);
            this.txt_recog_score.TabIndex = 1;
            this.txt_recog_score.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_recogScore_KeyPress);
            // 
            // switch_show_voice
            // 
            this.switch_show_voice.BackColor = System.Drawing.Color.Transparent;
            this.switch_show_voice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.switch_show_voice.Location = new System.Drawing.Point(105, 55);
            this.switch_show_voice.Margin = new System.Windows.Forms.Padding(0);
            this.switch_show_voice.MaximumSize = new System.Drawing.Size(52, 20);
            this.switch_show_voice.Name = "switch_show_voice";
            this.switch_show_voice.Size = new System.Drawing.Size(52, 20);
            this.switch_show_voice.SwitchStatus = false;
            this.switch_show_voice.TabIndex = 17;
            this.switch_show_voice.Click += new System.EventHandler(this.switch_show_voice_Click);
            // 
            // switch_show_text
            // 
            this.switch_show_text.BackColor = System.Drawing.Color.Transparent;
            this.switch_show_text.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.switch_show_text.Location = new System.Drawing.Point(105, 13);
            this.switch_show_text.Margin = new System.Windows.Forms.Padding(0);
            this.switch_show_text.MaximumSize = new System.Drawing.Size(52, 20);
            this.switch_show_text.Name = "switch_show_text";
            this.switch_show_text.Size = new System.Drawing.Size(52, 20);
            this.switch_show_text.SwitchStatus = false;
            this.switch_show_text.TabIndex = 14;
            this.switch_show_text.Click += new System.EventHandler(this.switch_show_text_Click);
            // 
            // cbx_pre_voice
            // 
            this.cbx_pre_voice.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.cbx_pre_voice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_pre_voice.Enabled = false;
            this.cbx_pre_voice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbx_pre_voice.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbx_pre_voice.FormattingEnabled = true;
            this.cbx_pre_voice.Items.AddRange(new object[] {
            "识别成功",
            "打卡成功"});
            this.cbx_pre_voice.Location = new System.Drawing.Point(105, 78);
            this.cbx_pre_voice.Name = "cbx_pre_voice";
            this.cbx_pre_voice.Size = new System.Drawing.Size(97, 25);
            this.cbx_pre_voice.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(36, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 64;
            this.label4.Text = "企业简称";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_company_name
            // 
            this.txt_company_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_company_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_company_name.EmptyTextTip = null;
            this.txt_company_name.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_company_name.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_company_name.Location = new System.Drawing.Point(127, 191);
            this.txt_company_name.MaxLength = 50;
            this.txt_company_name.Name = "txt_company_name";
            this.txt_company_name.Size = new System.Drawing.Size(339, 23);
            this.txt_company_name.TabIndex = 65;
            // 
            // BasicParamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(669, 490);
            this.Controls.Add(this.txt_company_name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_company_info);
            this.Controls.Add(this.btn_image);
            this.Controls.Add(this.pic_image_logo);
            this.Controls.Add(this.lbl_role);
            this.Controls.Add(this.lbl_dept);
            this.Controls.Add(this.txt_SDKKEY);
            this.Controls.Add(this.txt_APPID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_effective_date_tip);
            this.Controls.Add(this.lbl_recog_score_tip);
            this.Controls.Add(this.txt_recog_score);
            this.Controls.Add(this.lbl_effective_date_value);
            this.Controls.Add(this.lbl_active_status_value);
            this.Controls.Add(this.lbl_effective_date);
            this.Controls.Add(this.lbl_active_status);
            this.Controls.Add(this.lbl_recog_score);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BasicParamForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.BasicParamForm_Shown);
            this.panel_show.ResumeLayout(false);
            this.panel_show.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_image_logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private void btn_image_Click_1(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = false;
                openFileDialog.Title = "请选择图片";
                openFileDialog.Filter = "图片文件(*.jpg,*.png,*.bmp)|*.jpg;*.png;*.bmp";
                bool flag = openFileDialog.ShowDialog() == DialogResult.OK;
                if (flag)
                {
                    string fileName = openFileDialog.FileName;
                    float num = 5242880f;
                    float num2 = 2097152f;
                    FileInfo fileInfo = new FileInfo(fileName);
                    bool flag2 = (float)fileInfo.Length > num2;
                    if (flag2)
                    {
                        using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
                        {
                            byte[] bytes = Utils.ImageUtil.CompressionImage(fileStream, 50L, false);
                            bool flag3 = !Directory.Exists(this.tempDir);
                            if (flag3)
                            {
                                Directory.CreateDirectory(this.tempDir);
                            }
                            Random random = new Random();
                            string text = this.tempDir + random.Next(1000, 9999) + ".jpg";
                            File.WriteAllBytes(text, bytes);
                            openFileDialog.FileName = text;
                            fileInfo = new FileInfo(text);
                            fileStream.Dispose();
                            fileStream.Close();
                        }
                    }
                    bool flag4 = (float)fileInfo.Length > num;
                    if (flag4)
                    {
                        ErrorDialogForm.ShowErrorMsg("图片文件大小不得超过5MB!", "", false, null, false);
                    }

                    this.pic_image_logo.ImageLocation = openFileDialog.FileName;
                    this.pic_image_logo.Tag = openFileDialog.FileName;
                    this.pic_image_logo.SizeMode = PictureBoxSizeMode.Zoom;

                }
            }
            catch (Exception se)
            {
                LogHelper.LogError(base.GetType(), se);
            }
        }

	}
}
