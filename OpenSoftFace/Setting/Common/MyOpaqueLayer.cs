using OpenSoftFace.Setting.Forms;
using LogUtil;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Common
{
	internal class MyOpaqueLayer
	{
		private Form form = null;

		public void ShowOpaqueLayer(DragBaseForm control, double alpha, Color backColor)
		{
			try
			{
				bool flag = this.form == null;
				if (flag)
				{
					this.form = new Form();
					this.form.Opacity = alpha;
					this.form.BackColor = backColor;
					this.form.FormBorderStyle = FormBorderStyle.None;
					this.form.ShowInTaskbar = false;
					this.form.TopMost = true;
					control.AddOwnedForm(this.form);
					this.form.BringToFront();
					this.form.Show();
				}
				this.form.Width = control.Width;
				this.form.Height = control.Height;
				this.form.Location = control.Location;
				this.form.Enabled = true;
				this.form.Visible = true;
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void HideOpaqueLayer()
		{
			try
			{
				bool flag = this.form != null;
				if (flag)
				{
					this.form.Visible = false;
					this.form.Enabled = false;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}
	}
}
