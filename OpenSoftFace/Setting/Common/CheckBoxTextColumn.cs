using System;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Common
{
	public class CheckBoxTextColumn : DataGridViewCheckBoxColumn
	{
		private string text;

		public string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		public override DataGridViewCell CellTemplate
		{
			get
			{
				return new CheckBoxTextCell();
			}
		}
	}
}
