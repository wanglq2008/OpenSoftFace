using System;

namespace OpenSoftFace.Setting.Models
{
	public enum DetectionMode : uint
	{
		ASF_DETECT_MODE_VIDEO,
		ASF_DETECT_MODE_IMAGE = 4294967295u
	}
}
