using System;
using System.Runtime.InteropServices;

namespace OpenSoftFace.Setting.Models
{
	[StructLayout(LayoutKind.Sequential, Size = 1)]
	public struct FaceEngineMask
	{
		public const int ASF_NONE = 0;

		public const int ASF_FACE_DETECT = 1;

		public const int ASF_FACERECOGNITION = 4;

		public const int ASF_AGE = 8;

		public const int ASF_GENDER = 16;

		public const int ASF_FACE3DANGLE = 32;

		public const int ASF_LIVENESS = 128;

		public const int ASF_IR_LIVENESS = 1024;
	}
}
