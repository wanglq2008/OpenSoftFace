using System;

namespace OpenSoftFace.Setting.Models
{
	public struct MRECT
	{
		public int left;

		public int top;

		public int right;

		public int bottom;
	}
}
