using System;

namespace OpenSoftFace.Setting.Entity
{
	public class ImageInfo
	{
		public IntPtr imgData
		{
			get;
			set;
		}

		public int width
		{
			get;
			set;
		}

		public int height
		{
			get;
			set;
		}

		public int format
		{
			get;
			set;
		}
	}
}
