using OpenSoftFace.Setting.Models;
using System;

namespace OpenSoftFace.Setting.Entity
{
	public class FaceTrackUnit
	{
		public string message = string.Empty;

		public int liveness = -1;

		public MRECT Rect
		{
			get;
			set;
		}

		public IntPtr Feature
		{
			get;
			set;
		}
	}
}
