using OpenSoftFace.Setting.Common;
using CCWin.SkinControl;
using CommonConstant;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms
{
	public partial class DragBaseForm : Form
	{
		private MyOpaqueLayer cmd;

		private MyDragOpaqueLayer myDragLayer = new MyDragOpaqueLayer();


		public DragBaseForm()
		{
			this.InitializeComponent();
			this.lbl_title.SendToBack();
		}

		private void skinLabel1_MouseDown(object sender, MouseEventArgs e)
		{
			Win32.ReleaseCapture();
			Win32.SendMessage(base.Handle, 274, 61458, 0);
		}

		public virtual void Close_btn_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		public void ShowOpaqueLayer()
		{
			this.cmd.ShowOpaqueLayer(this, 0.5, ColorUtil.Black);
		}

		public void HideOpaqueLayer()
		{
			this.cmd.HideOpaqueLayer();
		}

		private void DragBaseForm_Load(object sender, EventArgs e)
		{
			bool flag = !base.DesignMode;
			if (flag)
			{
				this.cmd = new MyOpaqueLayer();
			}
		}

		private void btn_hide_Click(object sender, EventArgs e)
		{
			base.WindowState = FormWindowState.Minimized;
		}

		private void btn_hide_MouseLeave(object sender, EventArgs e)
		{
		}

		private void btn_hide_MouseMove(object sender, MouseEventArgs e)
		{
		}

		public void ShowMyDragOpaqueLayer(DragBaseForm form)
		{
			try
			{
				this.myDragLayer.ShowOpaqueLayer(form, 0.3, ColorUtil.Black);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void HideMyDragOpaqueLayer()
		{
			try
			{
				this.myDragLayer.HideOpaqueLayer();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}
	
	}
}
