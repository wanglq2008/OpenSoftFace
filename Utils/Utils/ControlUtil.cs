using CCWin.SkinControl;
using LogUtil;
using System;
using System.Windows.Forms;

namespace Utils
{
	public class ControlUtil
	{
		public static void TextBoxAppend(TextBox txtBox, string appendText, object txtLock = null)
		{
			try
			{
				if (txtBox.IsHandleCreated)
				{
					if (txtLock != null)
					{
						lock (txtLock)
						{
							txtBox.BeginInvoke(new Action(delegate
							{
								txtBox.AppendText(appendText);
							}));
							goto IL_6C;
						}
					}
					txtBox.BeginInvoke(new Action(delegate
					{
						txtBox.AppendText(appendText);
					}));
				}
				IL_6C:;
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ControlUtil), se);
			}
		}

		public static void ProgressBarUpdate(SkinProgressBar progressBar, int value, object progressLock = null)
		{
			try
			{
				if (value > 100)
				{
					value = 100;
				}
				if (progressBar.IsHandleCreated)
				{
					if (progressLock != null)
					{
						lock (progressLock)
						{
							progressBar.BeginInvoke(new Action(delegate
							{
								progressBar.Value = value;
							}));
							goto IL_7E;
						}
					}
					progressBar.BeginInvoke(new Action(delegate
					{
						progressBar.Value = value;
					}));
				}
				IL_7E:;
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ControlUtil), se);
			}
		}

		public static void LabelTextUpdate(Label label, string txt)
		{
			try
			{
				if (label.IsHandleCreated)
				{
					label.BeginInvoke(new Action(delegate
					{
						label.Text = txt;
					}));
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ControlUtil), se);
			}
		}

		public static void ButtonTextUpdate(Button button, string txt)
		{
			try
			{
				if (button.IsHandleCreated)
				{
					button.BeginInvoke(new Action(delegate
					{
						button.Text = txt;
					}));
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ControlUtil), se);
			}
		}
	}
}
