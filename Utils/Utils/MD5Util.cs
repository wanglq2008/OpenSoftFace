using LogUtil;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Utils
{
	public class MD5Util
	{
		public static string strConfigAECKey = "vMzWYIdy0PqbaDDpygLHwevXW9dG5W";

		public static string strVersionAECKey = "Yks3GoRlEceJsR4Xfdr84qWRrKFxJn";

		public static string MD5(string str, int code)
		{
			StringBuilder stringBuilder = new StringBuilder(code);
			try
			{
				byte[] array = System.Security.Cryptography.MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(str));
				for (int i = (array.Length - code / 2) / 2; i < (array.Length + code / 2) / 2; i++)
				{
					stringBuilder.Append(array[i].ToString("x2"));
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(MD5Util), se);
			}
			return stringBuilder.ToString();
		}

		public static string MD5(byte[] str, int code)
		{
			StringBuilder stringBuilder = new StringBuilder(code);
			try
			{
				System.Security.Cryptography.MD5.Create();
				for (int i = (str.Length - code / 2) / 2; i < (str.Length + code / 2) / 2; i++)
				{
					stringBuilder.Append(str[i].ToString("x2"));
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(MD5Util), se);
			}
			return stringBuilder.ToString();
		}

		public static string MD5(string str)
		{
			return MD5Util.MD5(str, 32);
		}

		public static string NewRNGBase64Str(int len)
		{
			len = len / 4 * 3 - 2;
			byte[] array = new byte[len];
			new RNGCryptoServiceProvider().GetBytes(array);
			return Convert.ToBase64String(array);
		}

		public static string Base16Encrypt(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return "";
			}
			byte[] arg_1F_0 = Encoding.UTF8.GetBytes(s);
			StringBuilder stringBuilder = new StringBuilder();
			byte[] array = arg_1F_0;
			for (int i = 0; i < array.Length; i++)
			{
				byte b = array[i];
				stringBuilder.Append(b.ToString("x2"));
			}
			return stringBuilder.ToString();
		}

		public static string Base16Decrypt(string s)
		{
			if (string.IsNullOrEmpty(s) || s.Length % 2 > 0)
			{
				return "";
			}
			byte[] array = new byte[s.Length / 2];
			try
			{
				for (int i = 0; i < s.Length; i += 2)
				{
					array[i / 2] = Convert.ToByte(s.Substring(i, 2), 16);
				}
			}
			catch
			{
				return "";
			}
			return Encoding.UTF8.GetString(array);
		}

		public static string AESEncrypt(string plainText, string strAECKey)
		{
			string result;
			try
			{
				RijndaelManaged arg_11_0 = new RijndaelManaged();
				byte[] bytes = Encoding.UTF8.GetBytes(plainText);
				arg_11_0.Key = Encoding.UTF8.GetBytes(MD5Util.GetMD5(strAECKey));
				arg_11_0.IV = Encoding.UTF8.GetBytes(MD5Util.GetMD5(strAECKey).Substring(8, 16));
				arg_11_0.Mode = CipherMode.CBC;
				arg_11_0.Padding = PaddingMode.PKCS7;
				byte[] arg_68_0 = arg_11_0.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);
				StringBuilder stringBuilder = new StringBuilder();
				byte[] array = arg_68_0;
				for (int i = 0; i < array.Length; i++)
				{
					byte b = array[i];
					stringBuilder.AppendFormat("{0:x2}", b);
				}
				result = stringBuilder.ToString();
			}
			catch
			{
				result = plainText;
			}
			return result;
		}

		public static string AESDecrypt(string cipherText, string strAECKey)
		{
			string result;
			try
			{
				RijndaelManaged rijndaelManaged = new RijndaelManaged();
				int num = cipherText.Length / 2;
				byte[] array = new byte[num];
				for (int i = 0; i < num; i++)
				{
					int num2 = Convert.ToInt32(cipherText.Substring(i * 2, 2), 16);
					array[i] = (byte)num2;
				}
				rijndaelManaged.Key = Encoding.UTF8.GetBytes(MD5Util.GetMD5(strAECKey));
				rijndaelManaged.IV = Encoding.UTF8.GetBytes(MD5Util.GetMD5(strAECKey).Substring(8, 16));
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				byte[] bytes = rijndaelManaged.CreateDecryptor().TransformFinalBlock(array, 0, array.Length);
				result = Encoding.UTF8.GetString(bytes);
			}
			catch
			{
				result = cipherText;
			}
			return result;
		}

		public static string GetMD5(string str)
		{
			StringBuilder stringBuilder = new StringBuilder();
			byte[] array = System.Security.Cryptography.MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(str));
			for (int i = 0; i < array.Length; i++)
			{
				byte b = array[i];
				stringBuilder.Append(b.ToString("X2"));
			}
			return stringBuilder.ToString();
		}

		public static bool Is_Number(string str)
		{
			bool result = true;
			if (str == null || str.Trim() == "")
			{
				result = false;
			}
			else
			{
				byte[] bytes = new ASCIIEncoding().GetBytes(str);
				for (int i = 0; i < bytes.Length; i++)
				{
					if (bytes[i] < 47 || bytes[i] > 57)
					{
						result = false;
						break;
					}
				}
			}
			return result;
		}

		public static int get_StrLength(string _SourceStr)
		{
			int num = 0;
			for (int i = 0; i < _SourceStr.Length; i++)
			{
				if (MD5Util.Is_Haizi(_SourceStr.Substring(i, 1)))
				{
					num += 2;
				}
				else
				{
					num++;
				}
			}
			return num;
		}

		public static bool Is_Haizi(string _SourceStr)
		{
			return new Regex("[^x00-xff]").IsMatch(_SourceStr);
		}

		public static string ImageEncode(string path)
		{
			try
			{
				FileStream fileStream = new FileStream(path, FileMode.Open);
				try
				{
					long quality = 100L;
					if (fileStream.Length > 5242880L)
					{
						quality = 50L;
					}
					return MD5Util.MD5(ImageUtil.CompressionImage(fileStream, quality, false), 32);
				}
				catch (Exception se)
				{
					LogHelper.LogError(typeof(MD5Util), se);
				}
				finally
				{
					fileStream.Dispose();
					fileStream.Close();
				}
			}
			catch (Exception ex)
			{
				LogHelper.LogError(typeof(MD5Util), ex);
				Console.WriteLine(ex);
			}
			return null;
		}

		public static string ImageEncode(string path, string savePath)
		{
			try
			{
				FileStream fileStream = new FileStream(path, FileMode.Open);
				try
				{
					bool flag = fileStream.Length > 2097152L;
					byte[] array = ImageUtil.CompressionImage(fileStream, flag ? 80L : 100L, false);
					if (savePath != null)
					{
						if (!flag)
						{
							File.Copy(path, savePath);
						}
						else
						{
							File.WriteAllBytes(savePath, array);
						}
					}
					return MD5Util.MD5(array, 32);
				}
				catch (Exception se)
				{
					LogHelper.LogError(typeof(MD5Util), se);
				}
				finally
				{
					fileStream.Dispose();
					fileStream.Close();
				}
			}
			catch (Exception ex)
			{
				LogHelper.LogError(typeof(MD5Util), ex);
				Console.WriteLine(ex);
			}
			return null;
		}

		public static string ScaleImage(string imagePath, string filePath = null, int dstWidth = 1024, int dstHeight = 1024, bool isPNG = false)
		{
			Graphics graphics = null;
			Image image = null;
			try
			{
				image = Image.FromFile(imagePath);
				string result;
				if (image.Width <= dstWidth && image.Height <= dstHeight)
				{
					result = imagePath;
					return result;
				}
				float widthAndHeight = MD5Util.getWidthAndHeight(image.Width, image.Height, dstWidth, dstHeight);
				int num = (int)((float)image.Width * widthAndHeight);
				int num2 = (int)((float)image.Height * widthAndHeight);
				if (num % 4 != 0)
				{
					num -= num % 4;
				}
				Bitmap bitmap = new Bitmap(num, num2);
				graphics = Graphics.FromImage(bitmap);
				graphics.Clear(Color.Transparent);
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.SmoothingMode = SmoothingMode.HighQuality;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.DrawImage(image, new Rectangle((num - num) / 2, (num2 - num2) / 2, num, num2), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
				EncoderParameters encoderParameters = new EncoderParameters();
				long[] value = new long[]
				{
					100L
				};
				EncoderParameter encoderParameter = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, value);
				encoderParameters.Param[0] = encoderParameter;
				ImageCodecInfo encoder = ImageUtil.GetEncoder(isPNG ? ImageFormat.Png : ImageFormat.Jpeg);
				if (filePath != null)
				{
					if (encoder == null)
					{
						bitmap.Save(filePath, isPNG ? ImageFormat.Png : ImageFormat.Jpeg);
					}
					else
					{
						bitmap.Save(filePath, encoder, encoderParameters);
					}
				}
				result = filePath;
				return result;
			}
			catch (Exception arg_12F_0)
			{
				Console.WriteLine(arg_12F_0.Message);
			}
			finally
			{
				if (graphics != null)
				{
					graphics.Dispose();
				}
				if (image != null)
				{
					image.Dispose();
				}
			}
			return imagePath;
		}

		public static float getWidthAndHeight(int oldWidth, int oldHeigt, int newWidth, int newHeight)
		{
			float result;
			if (oldHeigt > oldWidth)
			{
				result = (float)newHeight * 1f / (float)oldHeigt * 1f;
			}
			else
			{
				result = (float)newWidth * 1f / (float)oldWidth * 1f;
			}
			return result;
		}
	}
}
