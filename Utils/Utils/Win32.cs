using System;
using System.Runtime.InteropServices;

namespace Utils
{
	public class Win32
	{
		public const int WM_CTLCOLOREDIT = 307;

		public const int WM_PAINT = 15;

		public const int WM_CREATE = 1;

		public const int HTCAPTION = 2;

		public const int WM_ACTIVATE = 6;

		public const int WM_NCCREATE = 129;

		public const int WM_NCCALCSIZE = 131;

		public const int WM_NCPAINT = 133;

		public const int WM_NCACTIVATE = 134;

		public const int WM_SYSCOMMAND = 274;

		public const int SC_MOVE = 61456;

		public const int AW_HOR_POSITIVE = 1;

		public const int AW_HOR_NEGATIVE = 2;

		public const int AW_VER_POSITIVE = 4;

		public const int AW_VER_NEGATIVE = 8;

		public const int AW_CENTER = 16;

		public const int AW_HIDE = 65536;

		public const int AW_ACTIVE = 131072;

		public const int AW_SLIDE = 262144;

		public const int AW_BLEND = 524288;

		[DllImport("user32")]
		public static extern bool AnimateWindow(IntPtr hwnd, int dwTime, int dwFlags);

		[DllImport("gdi32.dll")]
		public static extern int CreateRoundRectRgn(int x1, int y1, int x2, int y2, int x3, int y3);

		[DllImport("user32.dll")]
		public static extern int SetWindowRgn(IntPtr hwnd, int hRgn, bool bRedraw);

		[DllImport("gdi32.dll", CharSet = CharSet.Ansi)]
		public static extern int DeleteObject(int hObject);

		[DllImport("user32.dll")]
		public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

		[DllImport("user32.dll")]
		public static extern bool ReleaseCapture();

		[DllImport("user32.dll")]
		public static extern IntPtr GetWindowDC(IntPtr hwnd);

		[DllImport("user32.dll")]
		public static extern int ReleaseDC(IntPtr hwnd, IntPtr hdc);
	}
}
