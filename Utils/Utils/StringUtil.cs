using System;
using System.Text.RegularExpressions;

namespace Utils
{
	public class StringUtil
	{
		public const string ERROEREPLACETEXT = "设备连接异常,请点击\"设备信息\"页面\"确定\"按钮,重连设备!";

		public const string ERRORTEXT = "签名校验错误";

		public static int CheckSQLKey(string checkString)
		{
			int num = 0;
			string[] array = "insert,delete,update,select,commit".Split(new char[]
			{
				','
			});
			if (array != null && array.Length != 0)
			{
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string value = array2[i];
					if (checkString.Contains(value))
					{
						num++;
					}
				}
			}
			return num;
		}

		public static bool CheckStringKey(string checkString)
		{
			return checkString.Contains("'") || checkString.Contains("'");
		}

		public static int CommonCheckString(string checkString, string alterTag, int maxLength, out string errorMessage)
		{
			int num = 0;
			errorMessage = "";
			if (checkString.Length > maxLength)
			{
				num++;
				errorMessage = string.Concat(new object[]
				{
					alterTag,
					"不能超过",
					maxLength,
					"个字符!"
				});
			}
			if (StringUtil.CheckStringKey(checkString))
			{
				num++;
				errorMessage = "字符中不能含有单引号";
			}
			return num;
		}

		public static int CommonCheckStringAndNotEmpty(string checkString, string alterTag, int maxLength, out string errorMessage)
		{
			errorMessage = "";
			int num = 0;
			if (string.IsNullOrWhiteSpace(checkString) || string.IsNullOrWhiteSpace(checkString.Trim()))
			{
				num++;
				errorMessage = "请输入" + alterTag + "!";
			}
			else
			{
				num = StringUtil.CommonCheckString(checkString, alterTag, maxLength, out errorMessage);
			}
			return num;
		}

		public static bool IsIPAddress(string checkValue)
		{
			return new Regex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$").IsMatch(checkValue);
		}

		public static bool IsIPPartAddress(string checkValue)
		{
			return new Regex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){2}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$").IsMatch(checkValue);
		}

		public static bool IsMACAddress(string checkValue)
		{
			return new Regex("^[A-F0-9]{2}(:[A-F0-9]{2}){5}$").IsMatch(checkValue.Replace("-", ":").ToUpper());
		}

		public static bool IsNumeric(string value)
		{
			return Regex.IsMatch(value, "^[0-9]*[1-9][0-9]*$");
		}

		public static string SwitchAlterSign(string oldString)
		{
			return oldString.Replace("签名校验错误", "设备连接异常,请点击\"设备信息\"页面\"确定\"按钮,重连设备!");
		}

		public static bool IsSignatureVerificationError(string msgStr)
		{
			return "设备连接异常,请点击\"设备信息\"页面\"确定\"按钮,重连设备!".Equals(msgStr);
		}

		public static string CheckStringEmpty(string checkStr)
		{
			if (string.IsNullOrWhiteSpace(checkStr))
			{
				return "--";
			}
			return checkStr;
		}

		public static string Reverse(string original)
		{
			if (string.IsNullOrEmpty(original))
			{
				return string.Empty;
			}
			char[] expr_14 = original.ToCharArray();
			Array.Reverse(expr_14);
			return new string(expr_14);
		}

		public static int CompareValue(string value1, string value2)
		{
			int result;
			try
			{
				int num = Convert.ToInt32(value1);
				int num2 = Convert.ToInt32(value2);
				if (num > num2)
				{
					result = 1;
				}
				else if (num < num2)
				{
					result = -1;
				}
				else
				{
					result = 0;
				}
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		public static string GetDateValue(string originalStr)
		{
			if (string.IsNullOrWhiteSpace(originalStr))
			{
				return string.Empty;
			}
			string pattern = "\\d{4}-\\d{2}-\\d{2}";
			MatchCollection matchCollection = Regex.Matches(originalStr, pattern);
			if (matchCollection != null && matchCollection.Count > 0)
			{
				return matchCollection[0].Value.Trim().Replace("-", "/");
			}
			return string.Empty;
		}

		public static string GetTimeValue(string originalStr)
		{
			if (string.IsNullOrWhiteSpace(originalStr))
			{
				return string.Empty;
			}
			string pattern = "\\d{2}:\\d{2}:\\d{2}";
			MatchCollection matchCollection = Regex.Matches(originalStr, pattern);
			if (matchCollection != null && matchCollection.Count > 0)
			{
				return matchCollection[0].Value.Trim();
			}
			return string.Empty;
		}

		public static float getFloatValue(string originalStr)
		{
			float result = 0f;
			try
			{
				if (string.IsNullOrWhiteSpace(originalStr))
				{
					return result;
				}
				float.TryParse(originalStr, out result);
			}
			catch
			{
			}
			return result;
		}

		public static int SearchCharCount(string strContent, string searchChar)
		{
			int num = 0;
			try
			{
				string text = strContent;
				while (true)
				{
					int num2 = text.IndexOf(searchChar);
					if (num2 < 0)
					{
						break;
					}
					num++;
					text = text.Substring(num2 + 1);
				}
			}
			catch
			{
			}
			return num;
		}

		public static int CheckSqlIntValue(string value)
		{
			try
			{
				int result;
				if (string.IsNullOrEmpty(value))
				{
					result = 0;
					return result;
				}
				result = int.Parse(value);
				return result;
			}
			catch
			{
			}
			return 0;
		}
	}
}
