using System;

namespace CommonConstant
{
	public class StringConstant
	{
		public const string SPECIAL_CHAR = "/,*,+,#,',\"";

		public const string SPECIAL_CHAR_MESSAGE = "不能含有特殊字符/*+#'\"";

		public const string QUOTATION_MARK_MESSAGE = "字符中不能含有单引号";

		public const string SPECIAL_SQL = "insert,delete,update,select,commit";

		public const string SPECIAL_SQL_MESSAGE = "不能含有特殊字符insert,delete,update,select,commit";
	}
}
