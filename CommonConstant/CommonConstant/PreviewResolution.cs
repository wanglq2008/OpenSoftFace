using System;

namespace CommonConstant
{
	public class PreviewResolution
	{
		public static string VIEW_640_480 = "0";

		public static string VIEW_1280_720 = "1";
	}
}
