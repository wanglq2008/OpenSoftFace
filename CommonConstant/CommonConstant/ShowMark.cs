using System;

namespace CommonConstant
{
	public class ShowMark
	{
		public const string SHOW_NONE = "0";

		public const string SHOW_NAME = "1";

		public const string SHOW_SUBNAME = "2";
	}
}
