using Model.Entity;
using System;
using System.Collections.Generic;

namespace DAL.Service
{
	public interface IAttendanceRecordService
	{
		int Insert(AttendanceRecord record);

		int Delete(int recordID);

		int Update(AttendanceRecord record);

		AttendanceRecord Get(int recordID);

		List<AttendanceRecord> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1);

		int Count(string[] where = null, string groupBy = null, bool distinct = false);

		Dictionary<string, AttendanceRecord2> QueryForAttendRecord(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1);
	}
}
