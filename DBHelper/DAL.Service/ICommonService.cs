using System;

namespace DAL.Service
{
	public interface ICommonService
	{
		int ColumnCount(string tableName, string columnName, string columnType);

		int addColumn(string tableName, string columnName, string columnType, object defauleValue);

		bool isHaveTableByName(string tableName);

		int executeInsertOperation(string sql);
	}
}
