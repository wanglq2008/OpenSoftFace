using Model.Entity;
using System;
using System.Collections.Generic;

namespace DAL.Service
{
	public interface IFeatureInfoService
	{
		int Insert(FeatureInfo faceInfo);

		int Delete(int faceInfoID);

		int Update(FeatureInfo faceInfo);

		FeatureInfo Get(int faceInfoID);

		List<FeatureInfo> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1);

		List<FeatureInfo> QueryForListJoin(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1);

		int Count(string[] where = null, bool distinct = false);

		int Update(List<int> idList, int status);
	}
}
