using CommonConstant;
using LogUtil;
using System;
using System.Data.SQLite;
using Utils;

namespace DAL.Service.Impl
{
	public class CommonServiceImpl : ICommonService
	{
		private string connectionString;

		public CommonServiceImpl()
		{
			try
			{
				this.connectionString = new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString();
			}
			catch (Exception ex)
			{
				LogHelper.LogError(base.GetType(), ex);
			}
		}

		public int ColumnCount(string tableName, string columnName, string columnType)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sqlStr = string.Format(" select count(*) as columnCount from sqlite_master where name ='{0}' and (sql like '%\"{1}\" {2}%' or sql like '%{1} {2}%')", tableName, columnName, columnType);
					Console.WriteLine("sql:{0}", sqlStr);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlStr;
						SQLiteDataReader reader = command.ExecuteReader();
						int num = 0;
						if (reader.Read())
						{
							num = int.Parse(reader["columnCount"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return num;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int addColumn(string tableName, string columnName, string columnType, object defauleValue)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sqlStr = string.Format("alter table {0} add column {1} {2}  default {3};", new object[]
					{
						tableName,
						columnName,
						columnType,
						defauleValue
					});
					Console.WriteLine("sql:{0}", sqlStr);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sqlStr;
							int arg_7E_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_7E_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public bool isHaveTableByName(string tableName)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sqlStr = string.Format(" select count(*) as columnCount from sqlite_master where name ='{0}' and type = 'table'", tableName);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlStr;
						SQLiteDataReader reader = command.ExecuteReader();
						int num = 0;
						if (reader.Read())
						{
							num = int.Parse(reader["columnCount"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return num > 0;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return false;
		}

		public int executeInsertOperation(string sql)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_4D_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_4D_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}
	}
}
