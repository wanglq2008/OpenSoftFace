using System;
using System.Collections.Generic;

namespace Model.Entity
{
	[Serializable]
	public class MainInfo
	{
		public int ID
		{
			get;
			set;
		}

		public string PersonSerial
		{
			get;
			set;
		}

		public string PersonName
		{
			get;
			set;
		}

		public string ShowImageName
		{
			get;
			set;
		}

		public int PLeft
		{
			get;
			set;
		}

		public int PTop
		{
			get;
			set;
		}

		public int PRight
		{
			get;
			set;
		}

		public int PBottom
		{
			get;
			set;
		}

		public int POrient
		{
			get;
			set;
		}

		public int PValid
		{
			get;
			set;
		}

		public string AddTime
		{
			get;
			set;
		}

		public string UpdateTime
		{
			get;
			set;
		}

		public string Dept
		{
			get;
			set;
		}

		public string MonthAttendTotal
		{
			get;
			set;
		}

		public string MonthAbsenceTotal
		{
			get;
			set;
		}

		public string MonthTotal
		{
			get;
			set;
		}

		public List<AttendanceRecord2> attendRecordList
		{
			get;
			set;
		}
	}
}
